#pragma once
#include "../MartIncludes.h"
#include "Address.h"
class Address;
class Socket
{
public:
	Socket();
	~Socket();
	bool Open(unsigned short port);
	void Close();
	bool IsOpen() const;
	bool Send(const Address &destination, const void *data, int size);
	int Recieve(Address &sender, void *data, int size);

private:
	int handle;
};

