#include "Socket.h"


Socket::Socket()
{
	handle = 0;
}


Socket::~Socket()
{
	Close();
}

bool Socket::Open(unsigned short port)
{
	handle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (handle <= 0) {
		printf("Failed to create socket!\n");
		return false;
	}
	sockaddr_in address;
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons((unsigned short) port);
	if (bind(handle, (const sockaddr*)&address, sizeof(sockaddr_in)) < 0) {
		printf("Failed to bind socket!\n");
		Close();
		return false;
	}
#if PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX
	int nonBlocking = 1;
	if (fcntl(handle, F_SETFL, O_NONCLOCK, nonBlocking) == -1) {
		printf("Failed to set non-blocking socket!");
		Close();
		return false;
	}
#elif PLATFORM == PLATFORM_WINDOWS
	DWORD nonBlocking = 1;
	if (ioctlsocket(handle, FIONBIO, &nonBlocking) != 0) {
		printf("Failed to set non-blocking socket!");
		Close();
		return false;
	}
#endif
	return true;
}

void Socket::Close()
{
	if (socket != 0) {
#if PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX
		close(handle);
#elif PLATFORM == PLATFORM_WINDOWS
		closesocket(handle);
#endif
		handle = 0;
	}
}

bool Socket::IsOpen() const
{
	return socket != 0;
}

bool Socket::Send(const Address &destination, const void *data, int size)
{
	if (handle <= 0) return false;

	sockaddr_in address;
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = htonl(destination.GetAddress());
	address.sin_port = htons((unsigned short)destination.GetPort());

	int sent_bytes = sendto(handle, (const char*)data, size, 0, (sockaddr*)&address, sizeof(sockaddr_in));

	return sent_bytes == size;
}

int Socket::Recieve(Address &sender, void *data, int size)
{
	if (handle <= 0) return false;
#if PLATFORM == PLATFORM_WINDOWS
	typedef int socklen_t;
#endif
	sockaddr_in from;
	socklen_t fromLength = sizeof(from);

	int received_bytes = recvfrom(handle, (char*)data, size, 0, (sockaddr*)&from, &fromLength);

	if (received_bytes <= 0)
		return 0;

	unsigned int address = ntohl(from.sin_addr.s_addr);
	unsigned int port = ntohs(from.sin_port);

	sender = Address(address, port);

	return received_bytes;
}