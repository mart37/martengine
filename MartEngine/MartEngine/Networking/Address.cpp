#include "Address.h"
#include <sstream>

Address::Address()
{
	this->address = 0;
	this->port = 0;
}

Address::Address(int address, unsigned short port)
{
	this->address = address;
	this->port = port;
}

Address::Address(char *address, unsigned short port)
{
	if (std::string(address) == "localhost") address = "127.0.0.1";
	std::istringstream iss(address);
	std::vector<std::string> tokens;
	std::string token;
	while (std::getline(iss, token, '.')) {
		if (!token.empty())
			tokens.push_back(token);
	}
	unsigned char a = std::stoi(tokens[0]);
	unsigned char b = std::stoi(tokens[1]);
	unsigned char c = std::stoi(tokens[2]);
	unsigned char d = std::stoi(tokens[3]);

	this->address = (a << 24) | (b << 16) | (c << 8) | d;
	this->port = port;
}