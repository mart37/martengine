#pragma once
#include "../MartIncludes.h"
class Address
{
public:
	Address();
	Address(int address, unsigned short port);
	Address(char *address, unsigned short port);
	unsigned int GetAddress() const { return address; }
	unsigned short GetPort() const { return port; }


private:
	unsigned int address;
	unsigned short port;
};

