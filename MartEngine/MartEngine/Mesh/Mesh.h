#pragma once
#include "../MartIncludes.h"
namespace MartEngine {
	class Mesh
	{
	public:
		Mesh(std::string inName,int inVaoId, int mVertexCount);
		std::string & GetName() { return mName; }
		int GetVaoId() { return mVaoId; }
		int GetVertexCount() { return mVertexCount; }
	private:
		std::string mName;
		int mVaoId;
		int mVertexCount;
	};
	typedef std::shared_ptr< Mesh > MeshPtr;
}
