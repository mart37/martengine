#pragma once
#include "Mesh.h"
#include "../Singleton.h"
namespace MartEngine {
	class MMeshImporter 
		:public Singleton<MMeshImporter>
	{
	public:
		MeshPtr ImportTerrainMesh(const char* inFileName);
		MeshPtr ImportMartMesh(const char* inFileName);
	private:
		GLuint CreateVAO();
		void UnbindVAO();
		void StoreInAttribList(int attribNum, int coordinateSize, float data[], int numData);
		void BindIndicesBuffer(int buffer[], int size);
		static const std::string PATH;
	};
}
