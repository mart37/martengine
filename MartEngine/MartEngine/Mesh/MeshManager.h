#pragma once
#include "../Singleton.h"
#include <unordered_map>
#include "Mesh.h"
namespace MartEngine {
	class MeshManager
		:public Singleton<MeshManager>
	{
	public:
		void Clear();

		MeshPtr GetMesh(const std::string& inMeshName );
	
	private:
		std::unordered_map<std::string, MeshPtr> mMeshMap;
	};
}
