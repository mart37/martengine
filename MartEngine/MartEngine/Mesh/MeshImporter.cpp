#include <fstream>
#include <sstream>
#include <vector>

#include "../MartIncludes.h"
#include "MeshImporter.h"


namespace MartEngine {

	union IntFloat {
		uint32_t i;
		float f;
	};

	MeshPtr MMeshImporter::ImportTerrainMesh(const char* inFileName)
	{
		return nullptr;
	}

	MeshPtr MMeshImporter::ImportMartMesh(const char* inFileName)
	{
		std::ifstream input(PATH + std::string(inFileName));
		std::string line;
		getline(input, line);//MartMesh, make sure it isn't some other file
		if (line != "MartMesh") return nullptr;

		//used for converting hex to float
		std::stringstream hexss;
		union IntFloat hexval;

		//used for index
		int i;

		//position
		std::vector<float> positions;
		{
			getline(input, line);
			std::istringstream ss(line);
			std::string token;
			while (std::getline(ss, token, ',')) {
				hexss.clear();
				hexss.str(std::string(""));
				hexss << std::hex << token;
				hexss >> hexval.i;
				positions.push_back(hexval.f);
				//printf("%f\t%d\n", hexval.f,positions.size());
			}
		}
		
		//normal
		std::vector<float> normals;
		{
			getline(input, line);
			std::istringstream ss(line);
			std::string token;
			while (std::getline(ss, token, ',')) {
				hexss.clear();
				hexss.str(std::string(""));
				hexss << std::hex << token;
				hexss >> hexval.i;
				normals.push_back(hexval.f);
				//printf("%f\t%d\n", hexval.f, normals.size());
			}
		}

		//texcoord
		std::vector<float> texcoords;
		{
			getline(input, line);
			std::istringstream ss(line);
			std::string token;
			while (std::getline(ss, token, ',')) {
				hexss.clear();
				hexss.str(std::string(""));
				hexss << std::hex << token;
				hexss >> hexval.i;
				texcoords.push_back(hexval.f);
				//printf("%f\t%d\n", hexval.f, texcoords.size());
			}
		}

		//index
		std::vector<int> indices;
		{
			getline(input, line);
			std::istringstream ss(line);
			while (ss >> i) {
				indices.push_back(i);
				if (ss.peek() == ',') ss.ignore();
				//printf("%d\t%d\n", i, indices.size());
			}
		}
		//TODO: Write to binary file for fast loading
		/*
		std::ofstream outFile(PATH + "test.mmb", std::ios::out | std::ios::binary);
		{
			char buffer[1];
			buffer[0] = positions.size();
			outFile.write(buffer, sizeof(int));
		}
		{
			char buffer[256];
			for (int i = 0; i < positions.size(); i += 256){

			}
		}*/

		GLuint vaoId = CreateVAO();
		BindIndicesBuffer(&indices[0], indices.size());
		StoreInAttribList(0, 3, &positions[0], positions.size());
		StoreInAttribList(1, 3, &normals[0], normals.size());
		StoreInAttribList(2, 2, &texcoords[0], texcoords.size());
		UnbindVAO();
		return std::make_shared<Mesh>(PATH + std::string(inFileName), vaoId, indices.size());
	}

	void MMeshImporter::StoreInAttribList(int attribNum, int coordinateSize, float data[], int numData)
	{
		GLuint vboId;
		glGenBuffers(1, &vboId);
		glBindBuffer(GL_ARRAY_BUFFER, vboId);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float)*numData, data, GL_STATIC_DRAW);
		glVertexAttribPointer(attribNum, coordinateSize, GL_FLOAT, GL_FALSE, 0, (void*)0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void MMeshImporter::BindIndicesBuffer(int buffer[], int size)
	{
		GLuint vboId;
		glGenBuffers(1, &vboId);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboId);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int)*size, buffer, GL_STATIC_DRAW);
	}

	GLuint MMeshImporter::CreateVAO()
	{
		GLuint vaoId;
		glGenVertexArrays(1,&vaoId);
		glBindVertexArray(vaoId);
		return vaoId;
	}

	void MMeshImporter::UnbindVAO()
	{
		glBindVertexArray(0);
	}


	const std::string MMeshImporter::PATH = "src/model/";
}
