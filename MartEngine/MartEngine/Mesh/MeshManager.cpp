#include "MeshManager.h"
#include "MeshImporter.h"
namespace MartEngine {

	MeshPtr MeshManager::GetMesh( const std::string& inMeshName )
	{
		auto toReturn = mMeshMap.find(inMeshName);
		if (toReturn == mMeshMap.end()) {
			MeshPtr mesh = MMeshImporter::Get().ImportMartMesh(inMeshName.c_str());
			mMeshMap[inMeshName] = mesh;
		}
		return mMeshMap[inMeshName];
	}

	void MeshManager::Clear()
	{
		mMeshMap.clear();
	}

}