#pragma once
#include "../Memory/PoolAllocator.h"
#include "../Math/Math.h"
#include "Mesh.h"
#include "../Texture/Png.h"
namespace MartEngine {

	typedef PoolAllocator<256, 1024> MeshComponentPool;

	class MeshComponent
	{
	public:

		DECLARE_POOL_NEW_DELETE(MeshComponentPool);

		MeshComponent(MeshPtr inMesh, const std::string &inRendererName);

		void Render();

		MeshPtr GetMesh() const { return mMesh; }
		void SetMesh(MeshPtr inMesh) { mMesh = inMesh; }

		PngPtr GetTexture(int index) const { return mTexture[index]; }
		void SetTexture(PngPtr inTexture,int index) { mTexture[index] = inTexture; }

		Quaternion GetRotation() const { return mRotation; }
		void setRotation(Quaternion &inRotation) { mRotation = inRotation; }

		float GetScale() const { return mScale; }
		void setScale(float inScale) { mScale = inScale; }

		Vector3 GetTranslation() const { return mTranslation; }
		void setTranslation(Vector3 &inTranslation) { mTranslation = inTranslation; }

		Matrix4 GetTransformation() const { return mTransformationMatrix; }

		const std::string &GetRendererName() { return mRenderer; };

	private:
		MeshComponent(){}
		std::string mRenderer;
		Matrix4 mTransformationMatrix;
		Quaternion mRotation;
		float mScale;
		Vector3 mTranslation;

		MeshPtr mMesh;
		PngPtr mTexture[4];
	};
	typedef std::shared_ptr<MeshComponent> MeshComponentPtr;
}
