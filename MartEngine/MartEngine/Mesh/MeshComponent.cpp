#include "MeshComponent.h"

#include "../Shaders/StaticShader/StaticShader.h"
#include "../Shaders/ShaderRegistry.h"
#include "../Window/Renderer.h"
namespace MartEngine{
	MeshComponent::MeshComponent(MeshPtr inMesh, const std::string &inRendererName) :
		mMesh(inMesh),
		mRenderer(inRendererName)
	{}

	void MeshComponent::Render()
	{

		mTransformationMatrix = Matrix4::Identity;

		Matrix4 scale;
		scale.CreateScale(mScale);

		Matrix4 rotation;
		rotation.CreateFromQuaternion(mRotation);

		Matrix4 translation;
		translation.CreateTranslation(mTranslation);

		mTransformationMatrix.Multiply(scale);
		mTransformationMatrix.Multiply(rotation);
		mTransformationMatrix.Multiply(translation);
		
		ShaderRegistry::Get().Retrieve(mRenderer)->Render(this);
	}
}