#include "ShaderRegistry.h"

namespace MartEngine {

	void ShaderRegistry::Register(const std::string &name, const ShaderProgramPtr &toMap) {
		stringToShader.insert({ name,toMap });
	}

	ShaderProgramPtr ShaderRegistry::Retrieve(const std::string &name) {
		static int test = 0;
		ShaderProgramPtr spp = stringToShader[name];
		if (spp == nullptr) {
			return ShaderProgramPtr(nullptr);
		}
		else {
			return spp;
		}
	}

	std::vector<std::string> ShaderRegistry::GetRegisteredNames() {
		std::vector<std::string> toReturn;
		for (auto const &mapping : stringToShader) {
			toReturn.push_back(mapping.first);
		}
		return toReturn;
	}

	void ShaderRegistry::Clear() {
		stringToShader.clear();
	}
}