#include "StaticShader.h"

const std::string StaticShader::VERTEX_FILE = "Shaders/StaticShader/vertexShader.txt";
const std::string StaticShader::FRAGMENT_FILE = "Shaders/StaticShader/fragmentShader.txt";
StaticShader::StaticShader() : ShaderProgram(VERTEX_FILE,FRAGMENT_FILE)
{
	BindAttributes();
	LinkAndValidate();
	GetAllUniformLocations();
}


StaticShader::~StaticShader()
{
}

void StaticShader::Render(const MartEngine::MeshComponent *inMeshComponent)
{
	LoadTransformationMatrix(inMeshComponent->GetTransformation());
	glBindVertexArray(inMeshComponent->GetMesh()->GetVaoId());
	glBindBuffer(GL_ARRAY_BUFFER, inMeshComponent->GetMesh()->GetVaoId());
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	if (inMeshComponent->GetTexture(0) != nullptr) {
		glEnableVertexAttribArray(2);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, inMeshComponent->GetTexture(0)->GetTextureId());
	}
	glDrawElements(GL_TRIANGLES, inMeshComponent->GetMesh()->GetVertexCount(), GL_UNSIGNED_INT, 0);
	glDisableVertexAttribArray(0);
	if (inMeshComponent->GetTexture(0) != nullptr) {
		glDisableVertexAttribArray(2);
	}
	glDisableVertexAttribArray(1);
	glBindVertexArray(0);
}

void StaticShader::BindAttributes()
{
	ShaderProgram::BindAttribute(0, "position");
	ShaderProgram::BindAttribute(1, "normal");
	ShaderProgram::BindAttribute(2, "texcoord");
}

void StaticShader::GetAllUniformLocations()
{
	transformationMatrixLocation = ShaderProgram::GetUniformLocation("transformationMatrix");
	viewMatrixLocation = ShaderProgram::GetUniformLocation("viewMatrix");
	projectionMatrixLocation = ShaderProgram::GetUniformLocation("projectionMatrix");
	ambientLightColorLocation = ShaderProgram::GetUniformLocation("ambientLightColor");
	directionalLightColorLocation = ShaderProgram::GetUniformLocation("directionalLightColor");
	directionalLightDirectionLocation = ShaderProgram::GetUniformLocation("directionalLightDirection");
	fogDensityLocation = ShaderProgram::GetUniformLocation("fogDensity");
	fogGradientLocation = ShaderProgram::GetUniformLocation("fogGradient");
	fogColorLocation = ShaderProgram::GetUniformLocation("fogColor");
}

void StaticShader::LoadTransformationMatrix(const MartEngine::Matrix4 &matrix)
{
	ShaderProgram::LoadMatrix4(transformationMatrixLocation, matrix);
}

void StaticShader::LoadCamera(const MartEngine::Camera &camera)
{
	ShaderProgram::LoadMatrix4(viewMatrixLocation, camera.GetViewMatrix());
	ShaderProgram::LoadMatrix4(projectionMatrixLocation, camera.GetProjectionMatrix());
}

void StaticShader::LoadAmbientLight(const MartEngine::AmbientLight &ambientLight)
{
	ShaderProgram::LoadVector3(ambientLightColorLocation, ambientLight.GetColor());
}

void StaticShader::LoadDirectionalLight(const MartEngine::DirectionalLight &directionalLight)
{
	ShaderProgram::LoadVector3(directionalLightColorLocation, directionalLight.GetColor());
	ShaderProgram::LoadVector3(directionalLightDirectionLocation, directionalLight.GetDirection());
}

void StaticShader::LoadFog(const MartEngine::Fog &fog) {
	ShaderProgram::LoadFloat(fogDensityLocation, fog.getDensity());
	ShaderProgram::LoadFloat(fogGradientLocation, fog.getGradient());
	ShaderProgram::LoadVector3(fogColorLocation, fog.GetColor());
}
