#pragma once
#include "../ShaderProgram.h"
#include "../../Game/Camera.h"
#include "../../Game/Entities/AmbientLight.h"
#include "../../Game/Entities/DirectionalLight.h"
#include "../../Game/Entities/Fog.h"
class TerrainShader :
	public ShaderProgram
{
public:
	TerrainShader();
	~TerrainShader();

	void Render(const MartEngine::MeshComponent *mesh) override;

	void LoadTransformationMatrix(const MartEngine::Matrix4 &matrix);
	void LoadCamera(const MartEngine::Camera &camera);
	void LoadAmbientLight(const MartEngine::AmbientLight &ambientLight);
	void LoadDirectionalLight(const MartEngine::DirectionalLight &directionalLight);
	void LoadFog(const MartEngine::Fog &fog);

private:
	static const std::string VERTEX_FILE;
	static const std::string FRAGMENT_FILE;

	int transformationMatrixLocation;
	int viewMatrixLocation;
	int projectionMatrixLocation;
	int ambientLightColorLocation;
	int directionalLightColorLocation;
	int directionalLightDirectionLocation;
	int fogDensityLocation;
	int fogGradientLocation;
	int fogColorLocation;

protected:
	void BindAttributes() override;
	void GetAllUniformLocations() override;
};

