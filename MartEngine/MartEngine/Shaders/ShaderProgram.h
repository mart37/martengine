#pragma once
#include <memory>
#include <string>

#include "../Math/Math.h"
#include "../Mesh/MeshComponent.h"

class ShaderProgram
{
public:
	ShaderProgram(const std::string &vertexFile, const std::string &fragmentFile);
	virtual ~ShaderProgram();

	void LinkAndValidate();

	void Start();
	void Stop();

	virtual void Render(const MartEngine::MeshComponent *mesh) = 0;

private:
	int programId;
	int vertexId;
	int fragmentId;

	static int LoadShader(const std::string &fileName, int type);

protected:
	virtual void BindAttributes() = 0;
	void BindAttribute(int attribute, const std::string &variableName);

	virtual void GetAllUniformLocations() = 0;
	int GetUniformLocation(const std::string &uniformName);

	void LoadFloat(int location, float value);
	void LoadVector3(int location, const MartEngine::Vector3 &vector);
	void LoadBool(int location, bool value);
	void LoadMatrix4(int location, const MartEngine::Matrix4 &matrix);

};

typedef std::shared_ptr< ShaderProgram > ShaderProgramPtr;

