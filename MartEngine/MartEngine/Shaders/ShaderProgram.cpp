#include "ShaderProgram.h"

//openGL
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <fstream>
#include <sstream>

#include <vector>
#include "../Math/Math.h"

ShaderProgram::ShaderProgram(const std::string &vertexFile, const std::string &fragmentFile)
{
	vertexId = LoadShader(vertexFile, GL_VERTEX_SHADER);
	fragmentId = LoadShader(fragmentFile, GL_FRAGMENT_SHADER);
	programId = glCreateProgram();
	glAttachShader(programId, vertexId);
	glAttachShader(programId, fragmentId);
}

ShaderProgram::~ShaderProgram()
{
	Stop();
	glDetachShader(programId, vertexId);
	glDetachShader(programId, fragmentId);
	glDeleteShader(vertexId);
	glDeleteShader(fragmentId);
	glDeleteProgram(programId);
}

void ShaderProgram::LinkAndValidate()
{
	glLinkProgram(programId);
	glValidateProgram(programId);
}

int ShaderProgram::LoadShader(const std::string &fileName, int type) {
	GLuint shaderId = glCreateShader(type);
	std::string shaderString;
	std::ifstream file(fileName);
	std::stringstream buffer;
	buffer << file.rdbuf();
	std::string program = buffer.str();

	const GLchar *programText[1];
	programText[0] = program.c_str();

	GLint programTextLen[1];
	programTextLen[0] = program.length();
	
	glShaderSource(shaderId, 1, programText, programTextLen);
	glCompileShader(shaderId);

	GLint compiled;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_FALSE) {
		GLint maxLength = 0;
		glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &maxLength);
		std::vector<GLchar> errorLog(maxLength);
		glGetShaderInfoLog(shaderId, maxLength, &maxLength, &errorLog[0]);
		for (GLchar c: errorLog) { printf("%c",c); }
		glDeleteShader(shaderId);
	}
	return shaderId;
}

void ShaderProgram::BindAttribute(int attribute, const std::string &variableName)
{
	glBindAttribLocation(programId, attribute, variableName.c_str());
}

int ShaderProgram::GetUniformLocation(const std::string &uniformName)
{
	return glGetUniformLocation(programId, uniformName.c_str());
}

void ShaderProgram::LoadFloat(int location, float value)
{
	glUniform1f(location, value);
}

void ShaderProgram::LoadVector3(int location, const MartEngine::Vector3 &vector)
{
	glUniform3f(location, vector.GetX(), vector.GetY(), vector.GetZ() );
}

void ShaderProgram::LoadBool(int location, bool value)
{
	if (value) glUniform1f(location, 1.0f);
	else glUniform1f(location, 0.0f);
}

void ShaderProgram::LoadMatrix4(int location, const MartEngine::Matrix4 &matrix)
{
	glUniformMatrix4fv(location, 1, GL_FALSE, matrix.GetFloatArray());
}

void ShaderProgram::Start() { glUseProgram(programId); }
void ShaderProgram::Stop() { glUseProgram(0); }