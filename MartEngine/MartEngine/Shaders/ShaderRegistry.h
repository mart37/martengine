#pragma once

#include "ShaderProgram.h"
#include "../MartIncludes.h"
#include "../Singleton.h"
#include <unordered_map>
namespace MartEngine {
	class ShaderRegistry
		:public Singleton<ShaderRegistry>
	{
	public:
		void Register(const std::string &name, const ShaderProgramPtr &toMap);
		ShaderProgramPtr Retrieve(const std::string &name);
		std::vector<std::string> GetRegisteredNames();
		void Clear();

	private:
		std::unordered_map<std::string, ShaderProgramPtr> stringToShader;
	};
}
