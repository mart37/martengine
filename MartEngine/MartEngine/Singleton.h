#ifndef __SINGLETON_H_
#define __SINGLETON_H_

#define DECLARE_SINGLETON(SingletonClass) friend class Singleton<SingletonClass>;

template <class T>
class Singleton
{
private:
	static T* mInstance;
protected:
	Singleton() {}
public:
	static void Initialize() {
			mInstance = new T();
	}
	static void Destroy() {
		delete mInstance;
	}
	static T& Get() {
		if (mInstance == nullptr) {
			Initialize();
		}
		return *mInstance;
	}
};
template <class T> T* Singleton<T>::mInstance = 0;

#endif