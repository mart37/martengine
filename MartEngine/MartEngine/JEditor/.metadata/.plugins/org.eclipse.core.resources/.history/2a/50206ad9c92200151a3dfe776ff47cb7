package editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GraphicsConfiguration;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.media.j3d.AmbientLight;
import javax.media.j3d.Appearance;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.DirectionalLight;
import javax.media.j3d.GeometryArray;
import javax.media.j3d.Material;
import javax.media.j3d.Shape3D;
import javax.media.j3d.Texture;
import javax.media.j3d.Texture2D;
import javax.media.j3d.TextureAttributes;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TriangleArray;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.vecmath.Color3f;
import javax.vecmath.Color4f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import com.sun.j3d.utils.geometry.GeometryInfo;
import com.sun.j3d.utils.geometry.NormalGenerator;
import com.sun.j3d.utils.universe.SimpleUniverse;

public class MeshPreview extends JPanel {
	private static final long serialVersionUID = 8227255421905769682L;
	Canvas3D mCanvas;
	
	Box zoomBox;
	
	BranchGroup group;
	SimpleUniverse universe;
	GraphicsConfiguration config;
	
	Vector3f viewTranslation;
	Transform3D viewTransform;
	Transform3D rotation;
	
	double angle = 0;
	float x = 0f;
	float y = 0.5f;
	float z = 5f;
	
	JButton zoomOut;
	JButton zoomIn;
	
	JCheckBox spin;
	
	boolean alive = true;
	
	{
		viewTranslation = new Vector3f();
		viewTransform = new Transform3D();
		rotation = new Transform3D();
		zoomOut = new JButton("-");
		zoomOut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(z < 100) z += 1f;
			}
		});
		zoomIn = new JButton("+");
		zoomIn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(z > 1) z -= 1f;
			}
		});
		group = new BranchGroup();
		
		spin = new JCheckBox("Spin");
		spin.setSelected(true);
		
		zoomBox = Box.createHorizontalBox();
		zoomBox.add(spin);
		zoomBox.add(Box.createGlue());
		zoomBox.add(zoomOut);
		zoomBox.add(new JLabel("Zoom"));
		zoomBox.add(zoomIn);
	}
	
	MeshPreview(String mesh){
		setLayout(new BorderLayout());
		config = SimpleUniverse.getPreferredConfiguration();
		mCanvas = new Canvas3D(config);
		add(mCanvas);
		String[] split = mesh.split("/");
		add(new JLabel(split[split.length-1]),BorderLayout.NORTH);
		add(zoomBox, BorderLayout.SOUTH);
		
		universe = new SimpleUniverse(mCanvas);
		
		String positions[] = null;
		String indices[] = null;
		
		ArrayList<Point3f> points = new ArrayList<Point3f>();
		TriangleArray triangleMesh;
		
		try (BufferedReader br = new BufferedReader(new FileReader(mesh))) {
			String line;
			int counter = 0;
			while ((line = br.readLine()) != null) {
		    	if(counter == 1) {//Positional info
		    		positions = line.split(",");
		    	}
		    	else if (counter == 4) {//indices
		    		indices = line.split(",");
		    	}
		    	counter++;
			}
		} catch (FileNotFoundException e1) {e1.printStackTrace();} catch (IOException e1) {e1.printStackTrace();}
		
		for(int i = 0; i < positions.length; i = i+3) {
			points.add(new Point3f(hexToFloat(positions[i]), hexToFloat(positions[i+1]), hexToFloat(positions[i+2])));
		}
		
		triangleMesh = new TriangleArray(indices.length/3, TriangleArray.COORDINATES);
		for(int i = 0; i < indices.length; ++i) {
			triangleMesh.setCoordinate(i,points[Integer.valueOf(indices[i])]);
		}
		
		Point3f e = new Point3f(1.0f, 0.0f, 0.0f); // east
		Point3f s = new Point3f(0.0f, 0.0f, 1.0f); // south
		Point3f w = new Point3f(-1.0f, 0.0f, 0.0f); // west
		Point3f n = new Point3f(0.0f, 0.0f, -1.0f); // north
		Point3f t = new Point3f(0.0f, 0.721f, 0.0f); // top

		TriangleArray pyramidGeometry = new TriangleArray(18,TriangleArray.COORDINATES);
		pyramidGeometry.setCoordinate(0, e);
		pyramidGeometry.setCoordinate(1, t);
		pyramidGeometry.setCoordinate(2, s);

		pyramidGeometry.setCoordinate(3, s);
		pyramidGeometry.setCoordinate(4, t);
		pyramidGeometry.setCoordinate(5, w);

		pyramidGeometry.setCoordinate(6, w);
		pyramidGeometry.setCoordinate(7, t);
		pyramidGeometry.setCoordinate(8, n);

		pyramidGeometry.setCoordinate(9, n);
		pyramidGeometry.setCoordinate(10, t);
		pyramidGeometry.setCoordinate(11, e);

		pyramidGeometry.setCoordinate(12, e);
		pyramidGeometry.setCoordinate(13, s);
		pyramidGeometry.setCoordinate(14, w);

		pyramidGeometry.setCoordinate(15, w);
		pyramidGeometry.setCoordinate(16, n);
		pyramidGeometry.setCoordinate(17, e);
		GeometryInfo geometryInfo = new GeometryInfo(pyramidGeometry);
		NormalGenerator ng = new NormalGenerator();
		ng.generateNormals(geometryInfo);

		GeometryArray result = geometryInfo.getGeometryArray();
		
		Appearance appearance = new Appearance();
		Color3f color = new Color3f(Color.gray);
		Color3f black = new Color3f(0.0f, 0.0f, 0.0f);
		Color3f white = new Color3f(1.0f, 1.0f, 1.0f);
		Texture texture = new Texture2D();
		TextureAttributes texAttr = new TextureAttributes();
		texAttr.setTextureMode(TextureAttributes.MODULATE);
		texture.setBoundaryModeS(Texture.WRAP);
		texture.setBoundaryModeT(Texture.WRAP);
		texture.setBoundaryColor(new Color4f(0.0f, 1.0f, 0.0f, 0.0f));
		Material mat = new Material(color, black, color, white, 70f);
		appearance.setTextureAttributes(texAttr);
		appearance.setMaterial(mat);
		appearance.setTexture(texture);
		Shape3D shape = new Shape3D(result, appearance);
		group.addChild(shape);

		updateUniverse();
		
		// lights
		BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0),
				1000.0);
		Color3f light1Color = new Color3f(.7f, .7f, .7f);
		Vector3f light1Direction = new Vector3f(4.0f, -7.0f, -12.0f);
		DirectionalLight light1 = new DirectionalLight(light1Color, light1Direction);
		light1.setInfluencingBounds(bounds);
		group.addChild(light1);
		Color3f ambientColor = new Color3f(.4f, .4f, .4f);
		AmbientLight ambientLightNode = new AmbientLight(ambientColor);
		ambientLightNode.setInfluencingBounds(bounds);
		group.addChild(ambientLightNode);
		
		universe.addBranchGraph(group);
		new Thread(new Runnable() { 
			public void run() {
				while(alive) {
					try {
						Thread.sleep(30);
					} catch (InterruptedException e) {e.printStackTrace();}
					if(spin.isSelected()) {
						angle -= 0.02;
						if(angle < -Math.PI*2) angle = 0;
					}
					updateUniverse();
					repaint();
				}
			}
		}).start();
	}
	
	private void updateUniverse() {
		viewTranslation = new Vector3f();
		viewTransform = new Transform3D();
		rotation = new Transform3D();
		viewTranslation.z = z;
		viewTranslation.x = x;
		viewTranslation.y = y;
		viewTransform.setTranslation(viewTranslation);
		rotation.rotY(angle);
		rotation.mul(viewTransform);
		universe.getViewingPlatform().getViewPlatformTransform().setTransform(rotation);
		universe.getViewingPlatform().getViewPlatformTransform().getTransform(viewTransform);
	}
	
	public void removeNotify() {
	    super.removeNotify();
	    alive = false;
	}
	
	public float hexToFloat(String hex) {
		Long i = Long.parseLong(hex, 16);
        Float f = Float.intBitsToFloat(i.intValue());
        System.out.println(f);
        return f;
	}
}
