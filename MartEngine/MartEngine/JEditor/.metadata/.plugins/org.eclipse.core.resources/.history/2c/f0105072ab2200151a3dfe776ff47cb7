package editor;

import com.sun.j3d.utils.universe.*;
import com.sun.j3d.utils.geometry.*;

import javax.media.j3d.*;
import javax.vecmath.*;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

class ParseOptions {
	final static String[] PARSE_OPTIONS = {"REGISTERED_GAME_CLASSES","GAME_OBJECTS"};
	
	static boolean Contains(String msg) {
		for(String s : PARSE_OPTIONS) {
			if (s.equals(msg)) return true;
		}
		return false;
	}
}

public class EditorPanel extends JPanel{
	private static final long serialVersionUID = -3136640692335821678L;
	
	private Communicator mCommunicator;
	
	private String parseOption;
	
	ArrayList<String> registeredGameClasses;
	ArrayList<GameObject> objectsInLevel;

	ObjectEditor mObjectEditor;
	ObjectList mObjectList;
	ObjectAttributeList mObjectAttributeList;
	
	{
		registeredGameClasses = new ArrayList<String>();
		objectsInLevel = new ArrayList<GameObject>();
		mObjectEditor = new ObjectEditor();
		mObjectList = new ObjectList();
		mObjectAttributeList = new ObjectAttributeList();
	}
	
	EditorPanel() {
		setLayout(new BorderLayout());
		add(mObjectEditor,BorderLayout.CENTER);
		add(mObjectList,BorderLayout.WEST);
		add(mObjectAttributeList,BorderLayout.EAST);
	}
	
	public void setCommunicator(Communicator inCommunicator) {
		mCommunicator = inCommunicator;
		add(mCommunicator,BorderLayout.NORTH);
	}
	
	public void parse(String msg) {
		if(ParseOptions.Contains(msg)) {
			parseOption = msg;
			return;
		}
		if(parseOption.equals(ParseOptions.PARSE_OPTIONS[0])) {
			registeredGameClasses.add(msg);
		}
		else if (parseOption.equals(ParseOptions.PARSE_OPTIONS[1])) {
			String oName = "Unknown";
			ArrayList<ObjectAttribute> oAttributes = new ArrayList<ObjectAttribute>();
			String[] split = msg.split("\\|");
			int count = 0;
			while(count != split.length) {
				if(split[count].equals("Name")) {
					oName = split[count+1];
					count += 2;
				}
				else if(split[count].equals("Class")) {
					oAttributes.add(new ObjectAttribute("Class",split[count+1]));
					count += 2;
				}
				else if(split[count].equals("Model")) {
					oAttributes.add(new ObjectAttribute("Model",split[count+1]));
					count += 2;
				}
				else if(split[count].equals("Texture")) {
					oAttributes.add(new ObjectAttribute("Texture",split[count+1]));
					count += 2;
				}
				else if(split[count].equals("Float")) {
					Float f = Float.valueOf(split[count+2]);
					oAttributes.add(new ObjectAttribute(split[count+1],f));
					count += 3;
				}
				else if(split[count].equals("Vector")) {
					Float x = Float.valueOf(split[count+2]);
					Float y = Float.valueOf(split[count+3]);
					Float z = Float.valueOf(split[count+4]);
					oAttributes.add(new ObjectAttribute(split[count+1],new Vector(x,y,z)));
					count += 5;
				}
			}
			GameObject toAdd = new GameObject(oName,oAttributes);
			objectsInLevel.add(toAdd);
			mObjectList.addObject(toAdd);
		}
	}
	
	class ObjectEditor extends JPanel {
		private static final long serialVersionUID = 7529929647197054156L;
		Canvas3D mCanvas;
		
		{
			setLayout(new GridLayout(1,1));
			GraphicsConfiguration config = SimpleUniverse.getPreferredConfiguration();
			mCanvas = new Canvas3D(config);
			add(mCanvas);
			BranchGroup contents = new BranchGroup();
			SimpleUniverse universe = new SimpleUniverse(mCanvas);
			Point3f e = new Point3f(1.0f, 0.0f, 0.0f); // east
			Point3f s = new Point3f(0.0f, 0.0f, 1.0f); // south
			Point3f w = new Point3f(-1.0f, 0.0f, 0.0f); // west
			Point3f n = new Point3f(0.0f, 0.0f, -1.0f); // north
			Point3f t = new Point3f(0.0f, 0.721f, 0.0f); // top

			TriangleArray pyramidGeometry = new TriangleArray(18,
					TriangleArray.COORDINATES);
			pyramidGeometry.setCoordinate(0, e);
			pyramidGeometry.setCoordinate(1, t);
			pyramidGeometry.setCoordinate(2, s);

			pyramidGeometry.setCoordinate(3, s);
			pyramidGeometry.setCoordinate(4, t);
			pyramidGeometry.setCoordinate(5, w);

			pyramidGeometry.setCoordinate(6, w);
			pyramidGeometry.setCoordinate(7, t);
			pyramidGeometry.setCoordinate(8, n);

			pyramidGeometry.setCoordinate(9, n);
			pyramidGeometry.setCoordinate(10, t);
			pyramidGeometry.setCoordinate(11, e);

			pyramidGeometry.setCoordinate(12, e);
			pyramidGeometry.setCoordinate(13, s);
			pyramidGeometry.setCoordinate(14, w);

			pyramidGeometry.setCoordinate(15, w);
			pyramidGeometry.setCoordinate(16, n);
			pyramidGeometry.setCoordinate(17, e);
			GeometryInfo geometryInfo = new GeometryInfo(pyramidGeometry);
			NormalGenerator ng = new NormalGenerator();
			ng.generateNormals(geometryInfo);

			GeometryArray result = geometryInfo.getGeometryArray();
			
			// yellow appearance
			Appearance appearance = new Appearance();
			Color3f color = new Color3f(Color.yellow);
			Color3f black = new Color3f(0.0f, 0.0f, 0.0f);
			Color3f white = new Color3f(1.0f, 1.0f, 1.0f);
			Texture texture = new Texture2D();
			TextureAttributes texAttr = new TextureAttributes();
			texAttr.setTextureMode(TextureAttributes.MODULATE);
			texture.setBoundaryModeS(Texture.WRAP);
			texture.setBoundaryModeT(Texture.WRAP);
			texture.setBoundaryColor(new Color4f(0.0f, 1.0f, 0.0f, 0.0f));
			Material mat = new Material(color, black, color, white, 70f);
			appearance.setTextureAttributes(texAttr);
			appearance.setMaterial(mat);
			appearance.setTexture(texture);
			Shape3D shape = new Shape3D(result, appearance);
			group.addChild(shape);

			// above pyramid
			Vector3f viewTranslation = new Vector3f();
			viewTranslation.z = 3;
			viewTranslation.x = 0f;
			viewTranslation.y = .3f;
			Transform3D viewTransform = new Transform3D();
			viewTransform.setTranslation(viewTranslation);
			Transform3D rotation = new Transform3D();
			rotation.rotX(-Math.PI / 12.0d);
			rotation.mul(viewTransform);
			universe.getViewingPlatform().getViewPlatformTransform().setTransform(
					rotation);
			universe.getViewingPlatform().getViewPlatformTransform().getTransform(
					viewTransform);
			
			// lights
			BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0),
					1000.0);
			Color3f light1Color = new Color3f(.7f, .7f, .7f);
			Vector3f light1Direction = new Vector3f(4.0f, -7.0f, -12.0f);
			DirectionalLight light1 = new DirectionalLight(light1Color, light1Direction);
			light1.setInfluencingBounds(bounds);
			group.addChild(light1);
			Color3f ambientColor = new Color3f(.4f, .4f, .4f);
			AmbientLight ambientLightNode = new AmbientLight(ambientColor);
			ambientLightNode.setInfluencingBounds(bounds);
			group.addChild(ambientLightNode);
			
			universe.addBranchGraph(group);
			new Thread(new Runnable() { public void run() {while(true) repaint();}}).start();
		}
		
		public void preview(GameObject gameObject) {
			ImageIcon img = new ImageIcon("../../src/img/test.png");
			JLabel texturePreview = new JLabel(img);
			add(texturePreview);
			repaint();
		}
		
		public void edit(GameObject gameObject, ObjectAttribute objectAttribute) {
			
		}
		
	}
	
	class ObjectList extends JPanel {
		private static final long serialVersionUID = -3583967198619355368L;
		private Box buttonBox;
		
		{
			setLayout(new BorderLayout());
			setBorder(new TitledBorder(new EtchedBorder(), "Objects"));
			buttonBox = Box.createVerticalBox();
		    JScrollPane scroll = new JScrollPane(buttonBox);
		    scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		    scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		    add(scroll);
		    setPreferredSize(new Dimension(150,0));
		}
		
		public void addObject(GameObject toAdd) {
			JButton button = new JButton(toAdd.getName());
			button.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					mObjectEditor.preview(toAdd);
					mObjectAttributeList.setObject(toAdd);
				}
			});
			button.setPreferredSize(new Dimension(100,15));
			JPanel panel = new JPanel();
			panel.setLayout(new BorderLayout());
			panel.add(button);
			buttonBox.add(panel);
			buttonBox.add(Box.createVerticalStrut(3));
			this.revalidate();
		}
		
	}
	
	class ObjectAttributeList extends JPanel {
		private Box buttonBox;
		private static final long serialVersionUID = -3635594832589618757L;
		
		{
			setLayout(new BorderLayout());
			setBorder(new TitledBorder(new EtchedBorder(), "Attributes"));
			buttonBox = Box.createVerticalBox();
		    JScrollPane scroll = new JScrollPane(buttonBox);
		    scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		    scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		    add(scroll);
		    setPreferredSize(new Dimension(150,0));
		}
		
		public void setObject(GameObject object) {
			buttonBox.removeAll();
			for(ObjectAttribute oa : object.getAttributes()) {
				JButton button = new JButton(oa.getName());
				button.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e) {
						mObjectEditor.edit(object,oa);
					}
				});
				JPanel panel = new JPanel();
				panel.setLayout(new BorderLayout());
				panel.add(button);
				buttonBox.add(panel);
				buttonBox.add(Box.createVerticalStrut(3));
				this.revalidate();
			}
		}
	}
}
