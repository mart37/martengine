package editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.media.j3d.AmbientLight;
import javax.media.j3d.Appearance;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.DirectionalLight;
import javax.media.j3d.GeometryArray;
import javax.media.j3d.Material;
import javax.media.j3d.Shape3D;
import javax.media.j3d.Texture;
import javax.media.j3d.Texture2D;
import javax.media.j3d.TextureAttributes;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TriangleArray;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.vecmath.Color3f;
import javax.vecmath.Color4f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import com.sun.j3d.utils.geometry.GeometryInfo;
import com.sun.j3d.utils.geometry.NormalGenerator;
import com.sun.j3d.utils.universe.SimpleUniverse;

public class MeshPreview extends JPanel {
	private static final long serialVersionUID = 8227255421905769682L;
	Canvas3D mCanvas;
	
	Box zoomBox;
	
	BranchGroup group;
	SimpleUniverse universe;
	GraphicsConfiguration config;
	
	Vector3f viewTranslation;
	Transform3D viewTransform;
	Transform3D rotation;
	
	double angle = 0;
	double xangle = -Math.PI/4.0d;
	float x = 0f;
	float y = 0f;
	float z = 5f;
	
	JButton zoomOut;
	JButton zoomIn;
	
	JCheckBox spin;
	
	boolean alive = true;
	
	{
		viewTranslation = new Vector3f();
		viewTransform = new Transform3D();
		rotation = new Transform3D();
		zoomOut = new JButton(){
			private static final long serialVersionUID = -2609383673562168677L;
			@Override
			protected void paintComponent(Graphics g) {
				int size = 20;
				g.drawOval(((this.getSize().width)/2)-(size/2), ((this.getSize().height)/2)-(size/2), size, size);
			}
		};
		zoomOut.setOpaque(false);
		zoomOut.setContentAreaFilled(false);
		zoomOut.setBorderPainted(false);
		zoomOut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(z < 100) z += 1f;
			}
		});
		zoomIn = new JButton() {
			private static final long serialVersionUID = -2609383673562168677L;
			@Override
			protected void paintComponent(Graphics g) {
				int size = 20;
				int X = this.getSize().width;
				int Y = this.getSize().height;
				g.drawOval((X/2)-(size/2), (Y/2)-(size/2), size, size);
				g.drawLine((X/2)-(size/2), 0, (X/2), Y);
				g.drawLine(0, (Y/2), X, (Y/2));
			}
		};
		zoomIn.setOpaque(false);
		zoomIn.setContentAreaFilled(false);
		zoomIn.setBorderPainted(false);
		zoomIn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(z > 1) z -= 1f;
			}
		});
		
		group = new BranchGroup();
		
		spin = new JCheckBox("Spin");
		spin.setSelected(true);
		
		zoomBox = Box.createHorizontalBox();
		zoomBox.add(spin);
		zoomBox.add(Box.createGlue());
		zoomBox.add(zoomOut);
		zoomBox.add(new JLabel("Zoom"));
		zoomBox.add(zoomIn);
	}
	
	MeshPreview(String mesh){
		setLayout(new BorderLayout());
		config = SimpleUniverse.getPreferredConfiguration();
		mCanvas = new Canvas3D(config);
		add(mCanvas);
		String[] split = mesh.split("/");
		add(new JLabel(split[split.length-1]),BorderLayout.NORTH);
		add(zoomBox, BorderLayout.SOUTH);
		
		universe = new SimpleUniverse(mCanvas);
		
		String positions[] = null;
		String indices[] = null;
		
		ArrayList<Float> data = new ArrayList<Float>();
		ArrayList<Point3f> points = new ArrayList<Point3f>();
		TriangleArray triangleMesh;
		
		try (BufferedReader br = new BufferedReader(new FileReader(mesh))) {
			String line;
			int counter = 0;
			while ((line = br.readLine()) != null) {
		    	if(counter == 1) {//Positional info
		    		positions = line.split(",");
		    	}
		    	else if (counter == 4) {//indices
		    		indices = line.split(",");
		    	}
		    	counter++;
			}
		} catch (FileNotFoundException e1) {e1.printStackTrace();} catch (IOException e1) {e1.printStackTrace();}
		
		//find the highest and lowest values to get average to center object
		float high = hexToFloat(positions[0]);
		float low = hexToFloat(positions[0]);
		int xyz = 0;
		for(int i = 0; i < positions.length; ++i) {
			float f = hexToFloat(positions[i]);
			if(xyz%3 == 1) {//is it a y coordinate?
				if(high < f) high = f;
				if(low > f) low = f;
			}
			data.add(f);
			xyz++;
		}
		y = (high+low)/2.0f;
		
		//convert all the values to 3D points
		for(int i = 0; i < positions.length; i = i+3) {
			points.add(new Point3f(data.get(i), data.get(i+1), data.get(i+2)));
		}
		
		//load the mesh with the positional points
		triangleMesh = new TriangleArray(indices.length, TriangleArray.COORDINATES);
		for(int i = 0; i < indices.length; ++i) {
			triangleMesh.setCoordinate(i,points.get(Integer.valueOf(indices[i])));
		}

		//this is fancy - generates normals for us
		GeometryInfo geometryInfo = new GeometryInfo(triangleMesh);
		NormalGenerator ng = new NormalGenerator();
		ng.generateNormals(geometryInfo);

		GeometryArray result = geometryInfo.getGeometryArray();
		
		Appearance appearance = new Appearance();
		Color3f color = new Color3f(Color.gray);
		Color3f black = new Color3f(0.0f, 0.0f, 0.0f);
		Color3f white = new Color3f(1.0f, 1.0f, 1.0f);
		Texture texture = new Texture2D();
		TextureAttributes texAttr = new TextureAttributes();
		texAttr.setTextureMode(TextureAttributes.MODULATE);
		texture.setBoundaryModeS(Texture.WRAP);
		texture.setBoundaryModeT(Texture.WRAP);
		texture.setBoundaryColor(new Color4f(0.0f, 1.0f, 0.0f, 0.0f));
		
		Material mat = new Material(color, black, color, white, 70f);
		appearance.setTextureAttributes(texAttr);
		appearance.setMaterial(mat);
		appearance.setTexture(texture);
		Shape3D shape = new Shape3D(result, appearance);
		group.addChild(shape);

		updateUniverse();
		
		// lights
		BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0),1000.0);
		
		//Add two lights facing opposite one another, this is just to make sure the object is lit well
		Color3f light1Color = new Color3f(.7f, .7f, .7f);
		Vector3f light1Direction = new Vector3f(4.0f, -7.0f, -12.0f);
		DirectionalLight light1 = new DirectionalLight(light1Color, light1Direction);
		light1.setInfluencingBounds(bounds);
		group.addChild(light1);
		
		Color3f light2Color = new Color3f(.7f, .7f, .7f);
		Vector3f light2Direction = new Vector3f(-4.0f, 7.0f, 12.0f);
		DirectionalLight light2 = new DirectionalLight(light2Color, light2Direction);
		light2.setInfluencingBounds(bounds);
		group.addChild(light2);
		
		//Don't miss any spots, add a flat ambient light to everything
		Color3f ambientColor = new Color3f(.4f, .4f, .4f);
		AmbientLight ambientLightNode = new AmbientLight(ambientColor);
		ambientLightNode.setInfluencingBounds(bounds);
		group.addChild(ambientLightNode);
		
		universe.addBranchGraph(group);
		new Thread(new Runnable() { 
			public void run() {
				while(alive) {
					try {
						Thread.sleep(30);
					} catch (InterruptedException e) {e.printStackTrace();}
					if(spin.isSelected()) {
						angle -= 0.02;
						if(angle < -Math.PI*2) angle = 0;
					}
					updateUniverse();
					repaint();
				}
			}
		}).start();
	}
	
	private void updateUniverse() {
		viewTranslation = new Vector3f();
		viewTransform = new Transform3D();
		rotation = new Transform3D();
		viewTranslation.z = z;
		viewTranslation.x = x;
		viewTranslation.y = y;
		viewTransform.setTranslation(viewTranslation);
		rotation.rotY(angle);
		Transform3D rotationX = new Transform3D();
		rotationX.rotX(xangle);
		rotation.mul(rotationX);
		rotation.mul(viewTransform);
		universe.getViewingPlatform().getViewPlatformTransform().setTransform(rotation);
		universe.getViewingPlatform().getViewPlatformTransform().getTransform(viewTransform);
	}
	
	public void removeNotify() {
	    super.removeNotify();
	    alive = false;
	}
	
	public float hexToFloat(String hex) {
		Long i = Long.parseLong(hex, 16);
        Float f = Float.intBitsToFloat(i.intValue());
        return f;
	}
}
