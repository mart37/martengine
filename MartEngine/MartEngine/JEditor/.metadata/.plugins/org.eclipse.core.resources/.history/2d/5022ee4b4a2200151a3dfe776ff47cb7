package editor;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

class ParseOptions {
	final static String[] PARSE_OPTIONS = {"REGISTERED_GAME_CLASSES","GAME_OBJECTS"};
	
	static boolean Contains(String msg) {
		for(String s : PARSE_OPTIONS) {
			if (s.equals(msg)) return true;
		}
		return false;
	}
}

public class EditorPanel extends JPanel{
	private static final long serialVersionUID = -3136640692335821678L;
	
	private Communicator mCommunicator;
	
	private String parseOption;
	
	ArrayList<String> registeredGameClasses;
	ArrayList<GameObject> objectsInLevel;

	ObjectEditor mObjectEditor;
	ObjectList mObjectList;
	ObjectAttributeList mObjectAttributeList;
	
	{
		registeredGameClasses = new ArrayList<String>();
		objectsInLevel = new ArrayList<GameObject>();
		mObjectEditor = new ObjectEditor();
		mObjectList = new ObjectList();
		mObjectAttributeList = new ObjectAttributeList();
	}
	
	EditorPanel() {
		setLayout(new BorderLayout());
		add(mObjectEditor,BorderLayout.CENTER);
		add(mObjectList,BorderLayout.WEST);
		add(mObjectAttributeList,BorderLayout.EAST);
	}
	
	public void setCommunicator(Communicator inCommunicator) {
		mCommunicator = inCommunicator;
		add(mCommunicator,BorderLayout.NORTH);
	}
	
	public void parse(String msg) {
		if(ParseOptions.Contains(msg)) {
			parseOption = msg;
			return;
		}
		if(parseOption.equals(ParseOptions.PARSE_OPTIONS[0])) {
			registeredGameClasses.add(msg);
		}
		else if (parseOption.equals(ParseOptions.PARSE_OPTIONS[1])) {
			String oName = "Unknown";
			ArrayList<ObjectAttribute> oAttributes = new ArrayList<ObjectAttribute>();
			String[] split = msg.split("\\|");
			int count = 0;
			while(count < split.length) {
				if(split[count] == "Name") {
					oName = split[count+1];
					count += 2;
				}
				else if(split[count] == "Class") {
					oAttributes.add(new ObjectAttribute("Class",split[count+1]));
					count += 2;
				}
				else if(split[count] == "Model") {
					oAttributes.add(new ObjectAttribute("Model",split[count+1]));
					count += 2;
				}
				else if(split[count] == "Texture") {
					oAttributes.add(new ObjectAttribute("Texture",split[count+1]));
					count += 2;
				}
				else if(split[count] == "Float") {
					
				}
				else if(split[count] == "Vector") {
					
				}
			}
			/*String objectName = split[count++];
			String objectClass = split[count++];
			String model = split[count];
			String texture = split[count++];
			Float scale = Float.valueOf(split[count++]);
			Float rx = Float.valueOf(split[count++]);
			Float ry = Float.valueOf(split[count++]);
			Float rz = Float.valueOf(split[count++]);
			Vector rotation = new Vector(rx,ry,rz);
			Float tx = Float.valueOf(split[count++]);
			Float ty = Float.valueOf(split[count++]);
			Float tz = Float.valueOf(split[count++]);
			Vector translation = new Vector(tx,ty,tz);
			GameObject toAdd = new GameObject(objectClass,model,texture,scale,rotation,translation);*/
			objectsInLevel.add(toAdd);
			mObjectList.addObject(toAdd);
			revalidate();
			repaint();
		}
	}
	
	class ObjectEditor extends JPanel {
		private static final long serialVersionUID = 7529929647197054156L;
		
		{
			setLayout(new BorderLayout());
		}
		
	}
	
	class ObjectList extends JPanel {
		private static final long serialVersionUID = -3583967198619355368L;
		private Box buttonBox;
		
		{
			setLayout(new BorderLayout());
			setBorder(new TitledBorder(new EtchedBorder(), "Objects"));
			buttonBox = Box.createVerticalBox();
		    JScrollPane scroll = new JScrollPane(buttonBox);
		    scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		    scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		    add(scroll);
		    setPreferredSize(new Dimension(150,0));
		}
		
		public void addObject(GameObject toAdd) {
			JButton button = new JButton(toAdd.getName());
			button.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					mObjectAttributeList.setObject(toAdd);
				}
			});
			button.setPreferredSize(new Dimension(100,15));
			JPanel panel = new JPanel();
			panel.setLayout(new BorderLayout());
			panel.add(button);
			buttonBox.add(panel);
			buttonBox.add(Box.createVerticalStrut(3));
		}
		
	}
	
	class ObjectAttributeList extends JPanel {
		private Box buttonBox;
		private static final long serialVersionUID = -3635594832589618757L;
		
		{
			setLayout(new BorderLayout());
			setBorder(new TitledBorder(new EtchedBorder(), "Attributes"));
			buttonBox = Box.createVerticalBox();
		    JScrollPane scroll = new JScrollPane(buttonBox);
		    scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		    scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		    add(scroll);
		}
		
		public void setObject(GameObject object) {
			buttonBox.removeAll();
			for(ObjectAttribute oa : object.getAttributes()) {
				JButton button = new JButton(oa.getName());
				button.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e) {
						
					}
				});
				JPanel panel = new JPanel();
				panel.setLayout(new BorderLayout());
				panel.add(button);
				buttonBox.add(panel);
				buttonBox.add(Box.createVerticalStrut(3));
				this.revalidate();
			}
		}
	}
}
