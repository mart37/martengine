package editor;

public class ObjectAttribute {
	private String mName;
	private Object mAttribute;
	
	ObjectAttribute(String inName, Object inAttribute) {
		mName = inName;
		mAttribute = inAttribute;
	}
	
	public String getName() {
		return mName;
	}
	
	public Object getAttribute() {
		return mAttribute;
	}
}
