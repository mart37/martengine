package editor;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class TexturePreview extends JPanel {
	private static final long serialVersionUID = -8194685808074857920L;
	TexturePreview(String texture){
		setLayout(new BorderLayout());
		setMaximumSize(new Dimension(150,150));
		setMinimumSize(new Dimension(150,150));
		setPreferredSize(new Dimension(150,150));
		String[] split = texture.split("/");
		setBorder(new TitledBorder(new EtchedBorder(), split[split.length-1]));
		ImageIcon image = new ImageIcon(texture);
		add(new JPanel() {
			private static final long serialVersionUID = 4355050363060021482L;
			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
			    if (image != null)
			    {
			        g.drawImage(image.getImage(),0,0,this);
			    }
			}
		});
	}
}
