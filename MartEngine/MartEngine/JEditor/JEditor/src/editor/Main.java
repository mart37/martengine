package editor;

import javax.swing.SwingUtilities;

public class Main {
	public static String MART_ENGINE_PATH = "";
	public static void main(String[] args) {
		final String workingDirectory = System.getProperty("user.dir");
		String folders[] = workingDirectory.split("\\\\");
		for( String s : folders) {
			if(!s.equals("JEditor")) {
				MART_ENGINE_PATH += s;
				MART_ENGINE_PATH += "/";
			}
		}
		
		SwingUtilities.invokeLater(new Runnable (){
	        public void run (){
	            new EditorFrame();
	        }});
	}
}
