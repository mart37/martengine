package editor;

import java.awt.BorderLayout;
import java.sql.Timestamp;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.text.DefaultCaret;

public class MessageBox extends JPanel{

	private static final long serialVersionUID = -7418604347658325251L;
	private JTextArea display;
	
	MessageBox() {
		setLayout(new BorderLayout());
		setBorder(new TitledBorder(new EtchedBorder(), "Messages"));
		display = new JTextArea(5,5);
		DefaultCaret caret = (DefaultCaret)display.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
	    display.setEditable(false);
	    display.setLineWrap(true);
	    JScrollPane scroll = new JScrollPane(display);
	    scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	    scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	    add(scroll);
	}
	
	public void addMessage(String msg) {
		display.append("\n" + new Timestamp(System.currentTimeMillis()) +": "+ msg);
	}
}
