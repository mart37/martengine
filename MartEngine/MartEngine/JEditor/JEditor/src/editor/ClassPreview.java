package editor;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class ClassPreview extends JPanel {
	private static final long serialVersionUID = -3294879972092124063L;
	ClassPreview(String className){
		setLayout(new BorderLayout());
		setMaximumSize(new Dimension(150,150));
		setMinimumSize(new Dimension(150,150));
		setPreferredSize(new Dimension(150,150));
		setBorder(new TitledBorder(new EtchedBorder(), "Class"));
		JPanel center = new JPanel();
		center.add(new JLabel(className));
		add(center);
	}
}
