package editor;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class EditorFrame extends JFrame{
	private static final long serialVersionUID = -6354031085800538499L;
	
	private MessageBox mMessageBox;
	private EditorPanel mEditorPanel;
	
	{
		mMessageBox = new MessageBox();
		mEditorPanel = new EditorPanel();
		new Communicator(mEditorPanel, mMessageBox);
	}
	
	EditorFrame() {
		super("Mart Engine Editor");
		ImageIcon icon = new ImageIcon(Main.MART_ENGINE_PATH+"src/mart/MartEngineLogoTransparent.png");
		setIconImage(icon.getImage());
		setSize(640,480);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(mMessageBox,BorderLayout.SOUTH);
		add(mEditorPanel,BorderLayout.CENTER);
		setVisible(true);
	}
}
