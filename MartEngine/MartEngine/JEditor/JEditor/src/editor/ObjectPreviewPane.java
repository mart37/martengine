package editor;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class ObjectPreviewPane extends JPanel {
	private static final long serialVersionUID = -7206465902990927878L;
	
	GameObject mGameObject;
	JPanel attributeBox;
	
	{
		setLayout(new BorderLayout());
		setBorder(new TitledBorder(new EtchedBorder(), "Unknown"));
		attributeBox = new JPanel();
		attributeBox.setLayout(new GridLayout(0,2));
	    JScrollPane scroll = new JScrollPane(attributeBox);
	    scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	    scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	    add(scroll);
	}
	
	ObjectPreviewPane(GameObject inGameObject) {
		mGameObject = inGameObject;
		setBorder(new TitledBorder(new EtchedBorder(), mGameObject.getName()));
		for(ObjectAttribute oa : inGameObject.getAttributes()) {
			addAttribute(oa);
		}
	}
	
	public void addAttribute(ObjectAttribute toAdd) {
		Object attribute = toAdd.getAttribute();
		if(attribute instanceof String) {
			if(toAdd.getName().equals("Class")) {
				attributeBox.add(new ClassPreview((String) attribute));
			}
			if(toAdd.getName().equals("Model")) {
				attributeBox.add(new MeshPreview(Main.MART_ENGINE_PATH+(String) attribute));
			}
			if(toAdd.getName().equals("Texture")) {
				attributeBox.add(new TexturePreview(Main.MART_ENGINE_PATH+(String) attribute));
			}
		}
		else if(attribute instanceof Vector) {
			attributeBox.add(new VectorPreview(toAdd.getName(),(Vector)attribute));
		}
		else if(attribute instanceof Float) {
			attributeBox.add(new FloatPreview(toAdd.getName(),(Float)attribute));
		}
	}

}
