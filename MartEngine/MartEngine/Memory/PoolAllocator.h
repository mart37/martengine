#pragma once
#include "../Singleton.h"
#include "Memory.h"

namespace MartEngine{
	template <size_t block_size>
	struct PoolBlock
	{
		char _memory[block_size];

		PoolBlock<block_size>* _next;

	private:
#if _WIN32
		char _padding[8];
#else
		char _padding[4];
#endif

	public:
		PoolBlock()
		{
			_next = nullptr;
		}

		//disallow single PoolBlock
		void* operator new(size_t size){}
		void operator delete(void *ptr){}

		void* operator new[](size_t size)
		{
			return _aligned_malloc(size, 16);
		}

		void operator delete[](void *ptr)
		{
			_aligned_free(ptr);
		}
	};

	template <size_t block_size, unsigned int num_blocks>
	class PoolAllocator : public Singleton < PoolAllocator<block_size, num_blocks> >
	{
		DECLARE_SINGLETON(PoolAllocator);
	public:
		void StartUp();
		void ShutDown();

		void* Allocate(size_t size);
		void Free(void *ptr);

		unsigned int GetNumBlocksFree() { return iBlocksFree; }

	protected:
		PoolAllocator()
			: m_pPool(0)
			, m_pFreeList(0)
		{}

		PoolBlock<block_size>* m_pPool;
		PoolBlock<block_size>* m_pFreeList;

		unsigned int iBlocksFree;
	};

	//IMPLEMENTATION
	template<size_t block_size, unsigned int num_blocks>
	void PoolAllocator<block_size, num_blocks>::StartUp()
	{
		m_pPool = new PoolBlock<block_size>[num_blocks];
		for (int i = 0; i < num_blocks - 1; i++) {
			m_pPool[i]._next = &m_pPool[i + 1];
		}
		m_pPool[num_blocks - 1]._next = &m_pPool[0];
		m_pFreeList = m_pPool;
		iBlocksFree = num_blocks;
	}

	template<size_t block_size, unsigned int num_blocks>
	void PoolAllocator<block_size, num_blocks>::ShutDown()
	{
		delete[] m_pPool;
	}

	template<size_t block_size, unsigned int num_blocks>
	void *PoolAllocator<block_size, num_blocks>::Allocate(size_t size)
	{
		if (iBlocksFree == 0) {
			return nullptr;
		}
		else {
			PoolBlock<block_size> *toReturn = m_pFreeList;
			m_pFreeList = m_pFreeList->_next;
			iBlocksFree--;
			return toReturn;
		}
	}

	template<size_t block_size, unsigned int num_blocks>
	void PoolAllocator<block_size, num_blocks>::Free(void *ptr)
	{
		PoolBlock<block_size> *toAdd = static_cast<PoolBlock<block_size>*>(ptr);
		if (toAdd != nullptr) {
			toAdd->_next = m_pFreeList;
			m_pFreeList = toAdd;
			iBlocksFree++;
		}
	}
}

#define DECLARE_POOL_NEW_DELETE(PoolTemplate)		\
	static void* operator new(size_t size)			\
	{												\
		return PoolTemplate::Get().Allocate(size);	\
	}												\
	static void operator delete(void* ptr)			\
	{												\
		PoolTemplate::Get().Free(ptr);				\
	}