#pragma once
#include "MIncludes.h"
namespace MartEngine {
	class MTimer
	{
	public:
		MTimer() {
			Reset();
		}
		double GetDeltaTime() {}
		void Reset(){
			time = glfwGetTime();
		}
	private:
		double time;
	};
}
