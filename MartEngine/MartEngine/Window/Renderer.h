#pragma once
#include "../Singleton.h"
#include "../Mesh/MeshComponent.h"
namespace MartEngine {
	class Renderer
		:public Singleton<Renderer>
	{
		DECLARE_SINGLETON(Renderer);
	public:
		void RenderMeshComponent(const MeshComponent *inMeshComponent);

	};
}
