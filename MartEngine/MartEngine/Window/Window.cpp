#include "Window.h"

namespace MartEngine {

	void window_size_callback(GLFWwindow* window, int width, int height) {
		glViewport(0, 0, width, height);
	}

	bool Window::Initialize()
	{
		glfwWindowHint(GLFW_SAMPLES, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		window = glfwCreateWindow(mWidth, mHeight, "Mart Engine", NULL, NULL);
		glfwSetWindowSizeCallback(window, window_size_callback);
		
		if (!window) {
			fprintf(stderr, "Window failed to init");
			glfwTerminate();
			return false;
		}
		glfwMakeContextCurrent(window);
		glfwSwapInterval(1);
		glEnable(GL_CULL_FACE);
		glEnable(GL_BACK);
		return true;
	}

	void Window::Destroy()
	{
		glfwDestroyWindow(window);
		Singleton::Destroy();
	}

	void Window::Clear()
	{
		glEnable(GL_DEPTH_TEST);
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	}

	void Window::Prepare()
	{
		glClearColor(0.5f, 0.5f, 0.5f, 1);
	}

	void Window::SwapBuffers()
	{
		glfwSwapBuffers(window);
	}

	bool Window::Closed()
	{
		if (glfwWindowShouldClose(window)) return true;
		else return false;
	}
}