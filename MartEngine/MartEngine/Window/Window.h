#ifndef __WINDOW_H_
#define __WINDOW_H_

#include "../MartIncludes.h"

#include "../Singleton.h"
namespace MartEngine
{
	class Window : public Singleton<Window>
	{
		DECLARE_SINGLETON(Window);
	public:
		bool Initialize();
		void Destroy();
		void Clear();
		void Prepare();
		void SwapBuffers();
		bool Closed();

		int GetWidth() { return mWidth; }
		int GetHeight() { return mHeight; }

		bool isKeyPressed(int key) { return glfwGetKey(window, key) == GLFW_PRESS; }

	private:
		GLFWwindow* window;
		int mWidth = 640;
		int mHeight = 480;
	};
}
#endif
