#pragma once
#include "Level.h"

#include "../MartIncludes.h"
#include "../Timer/Timer.h"
namespace MartEngine
{
	class LevelManager
	{
	public:
		void Init();
		void Update();
		void Render();

		void SelectLevel(int inLevelId);

		void RunGameLoop();

	private:
		void HandleKeyEvents();
		Timer mTimer;
		LevelPtr mCurrentLevel;
		std::vector< std::function <LevelPtr() > > mMLevelCreators;
	};
}
