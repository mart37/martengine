#pragma once
#include "../MartIncludes.h"
#include "../Memory/Memory.h"
#include "../Singleton.h"

#include "GameObject.h"
#include "../Shaders/ShaderProgram.h"
#include "SceneGraph.h"
#include <map>
namespace MartEngine {
	class GameWorld :
		public Singleton<GameWorld>
	{
		DECLARE_SINGLETON(GameWorld)
		DECLARE_ALIGNED_NEW_DELETE
	public:
		void Clear();
		void Update(double deltaTime);

		void AddToWorld(GameObjectPtr inGameObject);

		bool LoadLevel(std::string &levelFileName);

		SceneGraphPtr GetSceneGraph(const std::string &type);

		std::set<GameObjectPtr>& GetGameObjects() { return mGameObjects; }

	private:
		void SpawnGameObject(int index, const minIni &ini);
		
		std::set<GameObjectPtr> mGameObjects;

		std::map<std::string, SceneGraphPtr> mSceneGraphs;

	};
}