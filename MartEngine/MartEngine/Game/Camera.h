#pragma once
#include "../MartIncludes.h"
#include "../Math/Math.h"
#include "../Window/Window.h"
namespace MartEngine {
	class Camera
	{
	public:

		Camera();

		void Update(double deltaTime);
		void UpdateViewMatrix();
		void UpdateProjectionMatrix(int width, int height, float nearPlane, float farPlane, float FOV);
		void UpdateViewProjectionMatrix();

		void MoveCamera(const Vector3 &movement);
		void RotateCamera(const Quaternion &rotation);

		void SetCamera(Vector3 inPosition) { mPosition = inPosition; };
		void fixCamera(Quaternion inRotation) { mRotation = inRotation; };

		Matrix4 GetViewProjectionMatrix() const { return mViewProjectionMatrix; }
		Matrix4 GetViewMatrix() const { return mViewMatrix; }
		Matrix4 GetProjectionMatrix() const { return mProjectionMatrix; }

	private:
		Vector3 mPosition;
		Quaternion mRotation;

		Matrix4 mViewMatrix;
		Matrix4 mProjectionMatrix;
		Matrix4 mViewProjectionMatrix;
	};

}