#include "LevelManager.h"
#include "../Window/Window.h"

//Levels
#include "Level_Test.h"

namespace MartEngine
{
	void LevelManager::Init()
	{
		mMLevelCreators.push_back([]{return std::make_shared<Level_Test>(); });

		SelectLevel(0);
	}

	void LevelManager::HandleKeyEvents()
	{
	//TODO
	}

	void LevelManager::Update() 
	{
		if (mCurrentLevel) {
			mCurrentLevel->Update(mTimer.GetDeltaTime());
		}
	}

	void LevelManager::Render()
	{
		if (mCurrentLevel) {
			mCurrentLevel->Render();
		}
	}

	void LevelManager::SelectLevel(int inLevelId)
	{
		mCurrentLevel = nullptr;
		mCurrentLevel = mMLevelCreators[inLevelId]();
	}

	void LevelManager::RunGameLoop()
	{
		
		while (!Window::Get().Closed()) {
			Update();
			Render();
			glfwPollEvents();
		}
	}
	
}