#pragma once
#include "../../Math/Math.h"
namespace MartEngine {
	class DirectionalLight {
	public:
		DirectionalLight(Vector3 inColor, Vector3 inDirection);

		void SetColor(Vector3 &inColor);
		Vector3 GetColor() const;

		void SetDirection(Vector3 &inDirection);
		Vector3 GetDirection() const;

	private:
		Vector3 mColor;
		Vector3 mDirection;
	};
	typedef std::shared_ptr<DirectionalLight> DirectionalLightPtr;
}