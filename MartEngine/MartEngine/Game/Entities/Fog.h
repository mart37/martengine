#pragma once

#include "../../Math/Math.h"

namespace MartEngine {
	class Fog {
	public:
		Fog(float density, float gradient, Vector3 color);

		void SetDensity(float inDensity) { mDensity = inDensity; }
		float getDensity() const { return mDensity; }

		void SetGradient(float inGradient) { mGradient = inGradient; }
		float getGradient() const { return mGradient; }

		void SetColor(Vector3 &inSkyColor) { mColor = inSkyColor; }
		Vector3 GetColor() const { return mColor; }

	private:
		float mDensity;
		float mGradient;
		Vector3 mColor;
	};
	typedef std::shared_ptr<Fog> FogPtr;
}