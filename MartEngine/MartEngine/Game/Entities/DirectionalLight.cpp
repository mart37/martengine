#include "DirectionalLight.h"

namespace MartEngine {
	DirectionalLight::DirectionalLight(Vector3 inColor, Vector3 inDirection) {
		SetColor(inColor);
		SetDirection(inDirection);
	}

	void DirectionalLight::SetColor(Vector3 &inColor) {
		mColor = inColor;
	}

	Vector3 DirectionalLight::GetColor() const{
		return mColor;
	}

	void DirectionalLight::SetDirection(Vector3 &inDirection) {
		mDirection = inDirection;
	}

	Vector3 DirectionalLight::GetDirection() const{
		return mDirection;
	}
}