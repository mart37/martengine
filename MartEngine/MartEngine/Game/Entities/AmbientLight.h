#pragma once
#include "../../Math/Math.h"
namespace MartEngine {
	class AmbientLight {
	public:
		AmbientLight(Vector3 inColor);

		void SetColor(Vector3 &inColor);
		Vector3 GetColor() const;

	private:
		Vector3 mColor;
	};
	typedef std::shared_ptr<AmbientLight> AmbientLightPtr;
}