#include "AmbientLight.h"

namespace MartEngine {
	AmbientLight::AmbientLight(Vector3 inColor) {
		SetColor(inColor);
	}

	void AmbientLight::SetColor(Vector3 &inColor) {
		mColor = inColor;
	}

	Vector3 AmbientLight::GetColor() const{
		return mColor;
	}
}