#include "Fog.h"

namespace MartEngine{
	Fog::Fog(float inDensity, float inGradient, Vector3 inSkyColor) {
		mDensity = inDensity;
		mGradient = inGradient;
		mColor = inSkyColor;
	}
}