#pragma once
#include "../Mesh/MeshComponent.h"

#define DECLARE_GAMEOBJECT(name) virtual const char* GetGameObjectClass() const {return #name;}

namespace MartEngine {
	class GameObject;
	typedef std::shared_ptr<GameObject> GameObjectPtr;
	typedef GameObjectPtr (*FactoryFunc)();
	class GameWorld;
	class GameClassRegistry;

	class GameObject
	{
	public:
		DECLARE_GAMEOBJECT(GameObject)

		GameObject();
		static FactoryFunc Construct(){ return[](){return GameObjectPtr(new GameObject()); }; }


		void AddComponent(MeshComponentPtr inMeshComponent);
		MeshComponentPtr GetMeshComponent() { return mMeshComponent; }
		
		virtual void LoadFromIniSection(int index, const minIni &ini);
		virtual void Update(double deltaTime);

		std::string& GetObjectName() { return mObjectName; }

	protected:
		std::string mObjectName;

	private:
		MeshComponentPtr mMeshComponent;
	};
}
