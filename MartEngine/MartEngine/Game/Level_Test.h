#pragma once
#include "Level.h"
#include "../Shaders/ShaderProgram.h"
#include "Entities/AmbientLight.h"
#include "Entities/DirectionalLight.h"
#include "Entities/Fog.h"

namespace MartEngine
{
	class Level_Test :
		public Level
	{
	public:
		Level_Test();
		virtual ~Level_Test();
		virtual void Update(double deltaTime) override;
		virtual void Render() override;

	private:
		void RegisterGameClasses() override;
		void RegisterGameShaders() override;

		AmbientLight *mAmbientLight;
		DirectionalLight *mDirectionalLight;
		Fog *mFog;
	};
}