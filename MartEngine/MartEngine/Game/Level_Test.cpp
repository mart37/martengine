#include "Level_Test.h"

#include "../Shaders/StaticShader/StaticShader.h"
#include "../Shaders/TerrainShader/TerrainShader.h"

namespace MartEngine
{
	Level_Test::Level_Test()
	{
		MeshManager::Get().Clear();
		RegisterGameClasses();
		RegisterGameShaders();
		GameWorld::Get().LoadLevel(std::string("Level_Test"));
		mAmbientLight = new AmbientLight(Vector3(0.3f, 0.3f, 0.3f));
		mDirectionalLight = new DirectionalLight(Vector3(0.3f, 0.3f, 0.3f), Vector3(0.f, -1.f, 0.f));
		mFog = new Fog(0.007f, 1.5f, Vector3(0.5f, 0.5f, 0.5f));

		mCamera = Camera();
		mCamera.UpdateProjectionMatrix(Window::Get().GetWidth(), Window::Get().GetHeight(), 0.1f, 1000.0f, 70.0f);
	}


	Level_Test::~Level_Test()
	{
		delete mAmbientLight;
		delete mDirectionalLight;
		delete mFog;
	}

	void Level_Test::Update(double deltaTime)
	{
		Edit();
		mCamera.Update(deltaTime);
		GameWorld::Get().Update(deltaTime);
	}

	void Level_Test::Render()
	{
		Window::Get().Clear();
		Window::Get().Prepare();

		StaticShader *staticShader = (StaticShader *)(ShaderRegistry::Get().Retrieve("StaticShader")).get();
		staticShader->Start();
		staticShader->LoadAmbientLight(*mAmbientLight);
		staticShader->LoadDirectionalLight(*mDirectionalLight);
		staticShader->LoadFog(*mFog);
		staticShader->LoadCamera(mCamera);
		SceneGraphPtr staticScene = GameWorld::Get().GetSceneGraph("StaticShader");
		if(staticScene.get() != nullptr)
		staticScene.get()->Render();
		staticShader->Stop();

		TerrainShader *terrainShader = (TerrainShader *)(ShaderRegistry::Get().Retrieve("TerrainShader")).get();
		terrainShader->Start();
		terrainShader->LoadAmbientLight(*mAmbientLight);
		terrainShader->LoadDirectionalLight(*mDirectionalLight);
		terrainShader->LoadFog(*mFog);
		terrainShader->LoadCamera(mCamera);
		SceneGraphPtr terrainScene = GameWorld::Get().GetSceneGraph("TerrainShader");
		if (terrainScene.get() != nullptr)
		terrainScene.get()->Render();
		terrainShader->Stop();

		Window::Get().SwapBuffers();
	}

	void Level_Test::RegisterGameClasses()
	{
		GameClassRegistry::Get().Register("GameObject", GameObject::Construct());
	}

	void Level_Test::RegisterGameShaders()
	{
		ShaderRegistry::Get().Register("StaticShader", ShaderProgramPtr(new StaticShader()));
		ShaderRegistry::Get().Register("TerrainShader", ShaderProgramPtr(new TerrainShader()));
	}
}