#include "SceneGraph.h"

namespace MartEngine {

	SceneGraph::SceneGraph() {};
	SceneGraph::~SceneGraph() {
		Clear();
	};

	void SceneGraph::Render()
	{
		int i = 0;
		for (MeshComponentPtr component : mComponents){
			component->Render();
		}
	}

	void SceneGraph::AddComponent(MeshComponentPtr inMeshComponent)
	{
		mComponents.insert(inMeshComponent);
	}

	void SceneGraph::RemoveComponent(MeshComponentPtr inMeshComponent)
	{
		mComponents.erase(inMeshComponent);
	}

	void SceneGraph::Clear()
	{
		mComponents.clear();
	}
}
