#pragma once
#include "../MartIncludes.h"
#include "Camera.h"
#include "GameWorld.h"
#include "GameClassRegistry.h"
#include "../Shaders/ShaderRegistry.h"
#include "../Mesh/MeshManager.h"


namespace MartEngine
{
	class Level
	{
	public:
		Level(){};
		virtual ~Level() {};
		virtual void Render() = 0;
		virtual void Update(double deltaTime) = 0;

	protected:
		Camera mCamera;

		bool editing = false;
		void Edit();
		void Editor();
		void Send();
		void Recv();

		virtual void RegisterGameClasses() = 0;
		virtual void RegisterGameShaders() = 0;
	};
	typedef std::shared_ptr<Level> LevelPtr;
}