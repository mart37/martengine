#include "GameClassRegistry.h"

namespace MartEngine {

	void GameClassRegistry::Register(const std::string &name, const FactoryFunc &toMap) {
		stringToObj.insert({name,toMap});
	}

	GameObjectPtr GameClassRegistry::Create(const std::string &name) {
		FactoryFunc fc = stringToObj[name];
		if (fc == nullptr) {
			return GameObjectPtr(nullptr);
		}
		else {
			return fc();
		}
	}

	std::vector<std::string> GameClassRegistry::GetRegisteredNames() {
		std::vector<std::string> toReturn;
		for (auto const &mapping : stringToObj) {
			toReturn.push_back(mapping.first);
		}
		return toReturn;
	}

	void GameClassRegistry::Clear() {
		stringToObj.clear();
	}
}