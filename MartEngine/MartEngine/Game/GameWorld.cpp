#include "GameWorld.h"

#include "GameClassRegistry.h"
#include "../Shaders/ShaderProgram.h"
#include "../Shaders/StaticShader/StaticShader.h"

#include "../MartIncludes.h"
#include <fstream>

namespace MartEngine{
	bool GameWorld::LoadLevel(std::string &levelFileName)
	{
		minIni ini("src/levels/"+levelFileName + ".ini");
		int sectionIndex = 0;
		std::string sectionName;
		do {
			sectionName = ini.getsection(sectionIndex);
			if (sectionName == "Terrain") {
			}
			else if (sectionName != "") {
				SpawnGameObject(sectionIndex, ini);
			}
			sectionIndex++;
		} while (sectionName.length() != 0);
		return true;
	}

	void GameWorld::Clear()
	{
		for (auto const &iter : mSceneGraphs) {
			//second is the scene graph
			iter.second.get()->Clear();
		}
	}

	void GameWorld::AddToWorld(GameObjectPtr inGameObject)
	{
		mGameObjects.insert(inGameObject);
		auto index = mSceneGraphs.find(inGameObject->GetMeshComponent()->GetRendererName());
		if (index == mSceneGraphs.end()) {
			SceneGraphPtr toAdd = SceneGraphPtr(new SceneGraph());
			mSceneGraphs[inGameObject->GetMeshComponent()->GetRendererName()] = toAdd;
		}
		SceneGraphPtr sceneGraph = mSceneGraphs[inGameObject->GetMeshComponent()->GetRendererName()];
		sceneGraph.get()->AddComponent(inGameObject.get()->GetMeshComponent());
	}

	SceneGraphPtr GameWorld::GetSceneGraph(const std::string &type) {
		return mSceneGraphs[type];
	}

	void GameWorld::SpawnGameObject(int index, const minIni &ini)
	{
		std::string section = ini.getsection(index);
		auto gameObj = GameClassRegistry::Get().Create(ini.gets(section, "Class"));
		gameObj->LoadFromIniSection(index, ini);
		AddToWorld(gameObj);
	}

	void GameWorld::Update(double deltaTime)
	{
		for (const GameObjectPtr &toUpdate : mGameObjects) {
			toUpdate->Update(deltaTime);
		}
	}
}