#pragma once
#include "../MartIncludes.h"
#include "../Mesh/MeshComponent.h"
namespace MartEngine {
	class SceneGraph
	{
	public:

		SceneGraph::SceneGraph();
		SceneGraph::~SceneGraph();

		void Render();
		void AddComponent(MeshComponentPtr inMeshComponent);
		void RemoveComponent(MeshComponentPtr inMeshComponent);
		void Clear();

	private:
		std::set<MeshComponentPtr> mComponents;
	};
	typedef std::shared_ptr<SceneGraph> SceneGraphPtr;

}