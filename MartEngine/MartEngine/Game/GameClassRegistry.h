#pragma once

#include "../MartIncludes.h"
#include "../Singleton.h"
#include "GameObject.h"
#include <unordered_map>

namespace MartEngine {
	class GameClassRegistry
		:public Singleton<GameClassRegistry>
	{
	public:
		void Register(const std::string &name, const FactoryFunc &toMap);
		GameObjectPtr Create(const std::string &name);
		std::vector<std::string> GetRegisteredNames();
		void Clear();

	private:
		std::unordered_map<std::string, FactoryFunc> stringToObj;
	};
}
