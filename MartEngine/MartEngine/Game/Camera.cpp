#include "Camera.h"

namespace MartEngine {

	Camera::Camera()
	{
		mRotation = Quaternion(Vector3(0.0f, 0.0f, 0.0f), 0.0f);
		mPosition = Vector3(0.f, 0.f, 0.f);
		mViewMatrix = Matrix4::Identity;
		mProjectionMatrix = Matrix4::Identity;
		mViewProjectionMatrix = Matrix4::Identity;
	}

	void Camera::Update(double deltaTime)
	{
		Vector3 deltaPosition = Vector3(0.f, 0.f, 0.f);
		Quaternion deltaRotation = Quaternion(Vector3(0.f, 0.f, 0.f),0.f);

		if (Window::Get().isKeyPressed(GLFW_KEY_W)) {
			Vector3 toMove = Vector3::Forward;
			toMove.Rotate(mRotation);
			deltaPosition += toMove;
		}
		if (Window::Get().isKeyPressed(GLFW_KEY_Q)) {
			Vector3 toMove = Vector3::Right;
			toMove.Rotate(mRotation);
			deltaPosition += toMove;
		}
		if (Window::Get().isKeyPressed(GLFW_KEY_E)) {
			Vector3 toMove = Vector3::Left;
			toMove.Rotate(mRotation);
			deltaPosition += toMove;
		}
		if (Window::Get().isKeyPressed(GLFW_KEY_A)) {
			deltaRotation.Multiply(Quaternion(Vector3(0.f, 1.f, 0.f), (float)-deltaTime));
		}
		if (Window::Get().isKeyPressed(GLFW_KEY_S)) {
			Vector3 toMove = Vector3::Backward;
			toMove.Rotate(mRotation);
			deltaPosition += toMove;
		}
		if (Window::Get().isKeyPressed(GLFW_KEY_D)) {
			deltaRotation.Multiply(Quaternion(Vector3(0.f, 1.f, 0.f), (float)deltaTime));
		}
		if (Window::Get().isKeyPressed(GLFW_KEY_LEFT_SHIFT)) {
			deltaPosition += Vector3::Up;
		}
		if (Window::Get().isKeyPressed(GLFW_KEY_SPACE)) {
			deltaPosition += Vector3::Down;
		}
		deltaPosition.Multiply((float)deltaTime);

		MoveCamera(deltaPosition);
		RotateCamera(deltaRotation);
		UpdateViewMatrix();
		UpdateViewProjectionMatrix();
	}

	void Camera::UpdateViewMatrix()
	{
		Vector3 cameraForward = Vector3::Forward;
		cameraForward.Rotate(mRotation);

		Vector3 cameraUp = Vector3::Up;
		cameraUp.Rotate(mRotation);

		mViewMatrix.CreateLookAt(mPosition, mPosition + cameraForward, cameraUp);
	}

	void Camera::UpdateProjectionMatrix(int width, int height, float nearPlane, float farPlane, float FOV)
	{
		mProjectionMatrix.CreateProjectionMatrix(width, height, nearPlane, farPlane, FOV);
	}

	void Camera::UpdateViewProjectionMatrix()
	{
		mViewProjectionMatrix = mViewMatrix;
		mViewProjectionMatrix.Multiply(mProjectionMatrix);
	}

	void Camera::MoveCamera(const Vector3 &movement)
	{
		mPosition += movement;
	}

	void Camera::RotateCamera(const Quaternion &rotation)
	{
		mRotation.Multiply(rotation);
	}
}