#include "Level.h"
#include <sstream>

namespace MartEngine
{
	void Level::Editor(){
		system("java -jar JEditor/JEditor.jar");
		printf("Editor has disconnected!\n");
		sleep(1000);
		editing = false;
	}

	void Level::Send() {
		Socket socket;
		socket.Open(0);
		Address address = Address("localhost", 30000);
		std::stringstream ss;
		sleep(1000);
		socket.Send(address, "REGISTERED_GAME_CLASSES", sizeof("REGISTERED_GAME_CLASSES"));
		for (std::string registeredClassName: GameClassRegistry::Get().GetRegisteredNames()) {
			socket.Send(address, registeredClassName.c_str(), registeredClassName.size());
		}
		socket.Send(address, "GAME_OBJECTS", sizeof("GAME_OBJECTS"));
		for (auto gameObject : GameWorld::Get().GetGameObjects()) {
			MeshComponentPtr toSend = gameObject->GetMeshComponent();

			std::string info = "";
			info += "Name|";
			info += gameObject->GetObjectName(); info += "|";
			info += "Class|";
			info += gameObject->GetGameObjectClass(); info += "|";
			info += "Model|";
			info += toSend->GetMesh()->GetName(); info += "|";
			info += "Texture|";
			info += toSend->GetTexture(0)->GetName(); info += "|";

			info += "Float|Scale|";
			ss << toSend->GetScale(); info += ss.str(); ss.str(""); ss.clear(); info += "|";

			Vector3 euler = toSend->GetRotation().GetEulerAngles();
			info += "Vector|Rotation|";
			ss << euler.GetX(); info += ss.str(); ss.str(""); ss.clear(); info += "|";
			ss << euler.GetY(); info += ss.str(); ss.str(""); ss.clear(); info += "|";
			ss << euler.GetZ(); info += ss.str(); ss.str(""); ss.clear(); info += "|";
			
			info += "Vector|Translation|";
			ss << toSend->GetTranslation().GetX(); info += ss.str(); ss.str(""); ss.clear(); info += "|";
			ss << toSend->GetTranslation().GetY(); info += ss.str(); ss.str(""); ss.clear(); info += "|";
			ss << toSend->GetTranslation().GetZ(); info += ss.str(); ss.str(""); ss.clear(); info += "|";

			socket.Send(address, info.c_str(), info.size());
			printf("%s\n", info.c_str());
		}
	}

	void Level::Recv() {
		Socket socket;
		socket.Open(30001);
		Address sender;
		unsigned char buffer[256];
		while (editing) {
			int bytesRead = socket.Recieve(sender, buffer, sizeof(buffer));
			sleep(100);
		}
		printf("No longer Listening\n");
	}

	void Level::Edit() {
		MeshManager::Get().GetMesh("floor.mm");
		if (Window::Get().isKeyPressed(GLFW_KEY_F1) && !editing) {
			editing = true;
			printf("Connecting to editor\n");
			std::thread{ &MartEngine::Level::Editor, this }.detach();
			std::thread{ &MartEngine::Level::Send, this }.detach();
			std::thread{ &MartEngine::Level::Recv, this }.detach();
		}
	}
}


/*
MeshPtr toSet = MeshManager::Get().GetMesh("floor.mm");
gameObject->GetMeshComponent()->SetMesh(toSet);
*/