#include "GameObject.h"
#include "../Mesh/MeshManager.h"
#include "../Texture/PngManager.h"

namespace MartEngine{
	GameObject::GameObject()
	{

	}

	void GameObject::Update(double deltaTime)
	{
		Quaternion spin = mMeshComponent->GetRotation();
		spin.Multiply(Quaternion(Vector3(0.0f, 1.0f, 0.0f), (float)deltaTime));
		mMeshComponent->setRotation(spin);
	}

	void GameObject::AddComponent(MeshComponentPtr inMeshComponent)
	{
		if (mMeshComponent == nullptr) {
			mMeshComponent = inMeshComponent;
		}
	}

	void GameObject::LoadFromIniSection(int index, const minIni &ini)
	{
		std::string section = ini.getsection(index);
		mObjectName = section;
		
		auto meshComponent = MeshComponentPtr(
			new MeshComponent(
				MeshManager::Get().GetMesh(
					ini.gets(section, "Mesh")
				),
				ini.gets(section,"Shader")
			)
		);

		meshComponent->SetTexture(
			PngManager::Get().GetPngTexture(
				ini.gets(section, "Texture")
			),
			0
		);

		meshComponent->setScale(
			ini.getf(section,"Scale")
		);

		meshComponent->setRotation(
			Quaternion(Vector3(
				ini.getf(section, "RotationX"),
				ini.getf(section, "RotationY"),
				ini.getf(section, "RotationZ")),
				ini.getf(section, "RotationAngle")
			)
		);

		meshComponent->setTranslation(
			Vector3(
				ini.getf(section, "PositionX"),
				ini.getf(section, "PositionY"),
				ini.getf(section, "PositionZ")
			)
		);

		AddComponent(meshComponent);

	}

}