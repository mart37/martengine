//Mart Engine by Matt Carey

#include "MartIncludes.h"

#include "Window/Window.h"
#include "Game/LevelManager.h"
#include "Mesh/MeshComponent.h"


bool InitializeSockets()
{
#if PLATFORM == PLATFORM_WINDOWS
	WSADATA WsaData;
	return WSAStartup(MAKEWORD(2, 2), &WsaData) == NO_ERROR;
#else
	return true;
#endif
}

void ShutdownSockets()
{
#if PLATFORM == PLATFORM_WINDOWS
	WSACleanup();
#endif
}

bool InitSubSystems() {

	if (!InitializeSockets()) {
		printf("Failed to initialize networking!\n");
		return false;
	}

	if (!glfwInit()) {
		fprintf(stderr, "GLFW did not init!");
		return false;
	}

	if (!MartEngine::Window::Get().Initialize()) {
		fprintf(stderr, "MWindow failed to init!");
		return false;
	}

	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "GLFW did not init!");
		return false;
	}

	MartEngine::MeshComponentPool::Initialize();
	MartEngine::MeshComponentPool::Get().StartUp();

	return true;
}

void ShutdownSubSystems() {
	ShutdownSockets();
	MartEngine::MeshComponentPool::Get().ShutDown();
	MartEngine::MeshComponentPool::Destroy();
	MartEngine::Window::Get().Destroy();
	glfwTerminate();
}

void RunLoop() {
	MartEngine::LevelManager levelManager;
	levelManager.Init();
	levelManager.RunGameLoop();
}

int main() {

	if (InitSubSystems()){
		RunLoop();
		ShutdownSubSystems();
	}
	//system("pause");
	return 0;
}