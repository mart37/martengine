#pragma once
#include <vector>
//header file not made by author, see at picopng.cpp
// picoPNG version 20101224
// Copyright (c) 2005-2010 Lode Vandevenne
extern int decodePNG(std::vector<unsigned char>& out_image, unsigned long& image_width, unsigned long& image_height, const unsigned char* in_png, size_t in_size, bool convert_to_rgba32 = true);