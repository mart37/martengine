#include "PngManager.h"
#include "Png.h"

#include <fstream>
#include <sstream>

#include "picopng.h"

namespace MartEngine {
	PngPtr PngManager::GetPngTexture(const std::string& inTextureName)
	{
		auto toReturn = mPngTextureMap.find(inTextureName);
		if (toReturn == mPngTextureMap.end()) {
			PngPtr texture = ImportPng(inTextureName.c_str());
			mPngTextureMap[inTextureName] = texture;
		}
		return mPngTextureMap[inTextureName];
	}

	void PngManager::Clear()
	{
		mPngTextureMap.clear();
	}

	PngPtr PngManager::ImportPng(const char *inTextureFileName)
	{
		std::string textureString;
		std::ifstream file(PATH + inTextureFileName, std::ios::in | std::ios::binary);
		std::stringstream buffer;
		buffer << file.rdbuf();
		textureString = buffer.str();

		std::vector<unsigned char> out;
		unsigned long size = 256;
		decodePNG(out, size, size, (unsigned char *)textureString.c_str(), textureString.length());

		GLuint texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, size, size, 0, GL_RGBA, GL_UNSIGNED_BYTE, &(out[0]));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glGenerateMipmap(GL_TEXTURE_2D);

		return std::make_shared<Png>(std::string(inTextureFileName), texture);
	}

	const std::string PngManager::PATH = "src/img/";
}