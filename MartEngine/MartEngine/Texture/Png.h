#pragma once
#include "../MartIncludes.h"
namespace MartEngine {
	class Png
	{
	public:
		Png(std::string inName, GLuint inTextureId);
		~Png();

		std::string GetName() { return mName; }
		GLuint GetTextureId() { return mTextureId; }

	private:
		std::string mName;
		GLuint mTextureId;
	};
	typedef std::shared_ptr< Png > PngPtr;
}