#include "Png.h"

namespace MartEngine {

	Png::Png(std::string inName, GLuint inTextureId)
		: mName(inName)
		, mTextureId(inTextureId)
	{
	}


	Png::~Png()
	{
		GLuint del[1];
		del[0] = mTextureId;
		glDeleteTextures(1,del);
	}

}