#pragma once
#include "../Singleton.h"
#include <unordered_map>
#include "Png.h"

namespace MartEngine {
	class PngManager
		:public Singleton<PngManager>
	{
	public:
		void Clear();

		PngPtr GetPngTexture(const std::string &inTextureName);

	private:
		std::unordered_map<std::string, PngPtr> mPngTextureMap;

		PngPtr ImportPng(const char *inTextureFileName);
		static const std::string PATH;
	};
}
