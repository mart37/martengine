#pragma once

#include "../MartIncludes.h"
namespace MartEngine
{
	class Timer
	{
	public:
		Timer() {
			Reset();
		}
		void Reset() {
			mTime = glfwGetTime();
		}
		double GetDeltaTime() {
			double deltaTime = mTime - glfwGetTime();
			Reset();
			return deltaTime;
		}
	private:
		double mTime;
	};
}