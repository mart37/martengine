function clearFile(file)
	local file = io.open(file,"w");
	io.output(file);
	io.write("");
	io.close(file);
end

function writeString(file,str)
	local file = io.open(file,"a");
	io.output(file);
	io.write(str .. "\n");
	io.close(file);
end

function writeTable(file,data)
	local file = io.open(file,"a");
	io.output(file);
	for k,v in pairs(data) do
		io.write(v..",");
	end
	io.write("\n");
	io.close(file);
end