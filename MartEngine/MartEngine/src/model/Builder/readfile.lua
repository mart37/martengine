function isvalidfile(file)
	local f = io.open(file,"rb");
	if f then
		f:close();
	end
	return f ~= nil;
end

function getlines(file)
	if not isvalidfile(file) then
		print("Error: " .. file .. " is not a valid file!");
		return {};
	end
  
	lines = {};
	for line in io.lines(file) do
		lines[#lines + 1] = line;
	end
	return lines;
end