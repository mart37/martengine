require 'readfile'

local datalineoffset = 4;

function trimString(str)
	return str:gsub("%s+","");
end

function remChars(str,chrs)
	return str:gsub("[" .. chrs .. "]", "");
end

function stripString(str)
	str = trimString(str);
	str = remChars(str,"{}");
	return str;
end

--types of data to extract from ogex
local datatypes = {
"position",
"normal",
"texcoord",
"IndexArray",
}

function initDataTable()
	local data = {};
	for k,v in pairs(datatypes) do
		data[v] = {};
	end
	return data;
end

function isDataType(str)
	for k,v in pairs(datatypes) do
		if string.find(str, v) then
			return v;
		end
	end
	return nil;
end

function process(fileName)
	local lines = getlines(fileName);
	if lines == nil then
		return nil;
	end
	local data = initDataTable();
	for k,v in pairs(lines) do
		datatype = isDataType(v);
		if datatype then
			index = k + datalineoffset;
			line = "-";
			while string.len(line) ~= 0 do
				line = lines[index];
				line = stripString(line);
				for number in string.gmatch(line, '([^,]+)') do
					if number:sub(2,2) == 'x' then --hex
						number = string.sub(number,3,-1);
					end
					data[datatype][#data[datatype] + 1] = number;
				end
				index = index + 1;
			end
		end
	end
	return data;
end