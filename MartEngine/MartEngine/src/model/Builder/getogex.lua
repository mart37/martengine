function getogex()
	local ogexfiles = {};
	local handle = io.popen("dir");
	local result = handle:read("*a");
	handle:close()
	for word in string.gmatch(result, "%S+") do
		if string.match(word, ".ogex") then
			ogexfiles[#ogexfiles+1] = word;
		end
	end
	return ogexfiles;
end