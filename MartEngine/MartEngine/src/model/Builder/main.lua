--Matt Carey

require 'getogex'
require 'process'
require 'writefile'

function pause()
	io.read();
end

function main()
	print("Matt Carey's ogex to martmesh script");
	local ogexfiles = getogex();
	for k,v in pairs(ogexfiles) do
		print ("Processing " .. v);
		local data = process(v);
		if data then
			print(v .. " has been processed!");
			local out = string.sub(v,1,-6) .. ".mm";
			clearFile(out);
			writeString(out, "MartMesh");
			writeTable(out, data["position"]);
			writeTable(out, data["normal"]);
			writeTable(out, data["texcoord"]);
			writeTable(out, data["IndexArray"]);
			print("File " .. out .. " has been created.");
		else
			print("Unable to process" .. v);
		end
	end
end

main();
pause();