#pragma once

//General Includes
#include <memory>
#include <cstdio>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <set>
#include <functional>
#include <thread>

//Platform
#include "platform.h"

//Networking
#include "networkIncludes.h"

//Thread sleep
#include "misc/sleep.h"

//openGL
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

//ini File Reading
#include "misc/ini/minIni.h"