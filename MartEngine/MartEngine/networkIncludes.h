#pragma once
#include "MartIncludes.h"
//CROSS-PLATFORM NETWORKING

#if PLATFORM == PLATFORM_WINDOWS
#include <winsock2.h>
#pragma comment( lib, "wsock32.lib" )
#elif PLATFORM == PLATFORM_MAC ||  PLATFORM == PLATFORM_UNIX
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#endif

//Socket and Address classes
#include "Networking/Socket.h"
#include "Networking/Address.h"