#include "Math.h"

namespace MartEngine
{
	float _identity[4][4] = { 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f };
	const Matrix4 Matrix4::Identity(_identity);

	const Vector3 Vector3::Zero(0.0f, 0.0f, 0.0f);
	const Vector3 Vector3::UnitX(1.0f, 0.0f, 0.0f);
	const Vector3 Vector3::UnitY(0.0f, 1.0f, 0.0f);
	const Vector3 Vector3::UnitZ(0.0f, 0.0f, 1.0f);
	const Vector3 Vector3::NegativeUnitX(-1.0f, 0.0f, 0.0f);
	const Vector3 Vector3::NegativeUnitY(0.0f, -1.0f, 0.0f);
	const Vector3 Vector3::NegativeUnitZ(0.0f, 0.0f, -1.0f);
	const Vector3 Vector3::Right(1.0f, 0.0f, 0.0f);
	const Vector3 Vector3::Up(0.0f, 1.0f, 0.0f);
	const Vector3 Vector3::Forward(0.0f, 0.0f, 1.0f);
	const Vector3 Vector3::Left(-1.0f, 0.0f, 0.0f);
	const Vector3 Vector3::Down(0.0f, -1.0f, 0.0f);
	const Vector3 Vector3::Backward(0.0f, 0.0f, -1.0f);

	void Vector3::Rotate(const Quaternion& q)
	{
		Vector3 temp(*this);
		temp.Multiply(q.GetScalar());
		temp.Add(Cross(q._qv, *this));

		temp = Cross(q._qv, temp);
		temp.Multiply(2.0f);
		temp.Add(*this);

		_x = temp._x;
		_y = temp._y;
		_z = temp._z;
		_w = temp._w;
	}

	void Matrix4::CreateTranslation(const Vector3 &rhs)
	{
		memset(_matrix, 0, sizeof(float)* 16);

		_matrix[0][0] = 1.0f;
		_matrix[3][0] = rhs.GetX();
		_matrix[1][1] = 1.0f;
		_matrix[3][1] = rhs.GetY();
		_matrix[2][2] = 1.0f;
		_matrix[3][2] = rhs.GetZ();
		_matrix[3][3] = 1.0f;
	}

	void Matrix4::CreateFromQuaternion(const Quaternion &q)
	{
		float x = q.GetVectorX();
		float y = q.GetVectorY();
		float z = q.GetVectorZ();
		float w = q.GetScalar();

		_matrix[0][0] = 1.0f - 2.0f*y*y - 2.0f*z*z;
		_matrix[1][0] = 2.0f*x*y - 2.0f*z*w;
		_matrix[2][0] = 2.0f*x*z + 2.0f*y*w;
		_matrix[3][0] = 0.0f;

		_matrix[0][1] = 2.0f*x*y + 2.0f*z*w;
		_matrix[1][1] = 1.0f - 2.0f*x*x - 2.0f*z*z;
		_matrix[2][1] = 2.0f*y*z - 2.0f*x*w;
		_matrix[3][1] = 0.0f;

		_matrix[0][2] = 2.0f*x*z - 2.0f*y*w;
		_matrix[1][2] = 2.0f*y*z + 2.0f*x*w;
		_matrix[2][2] = 1.0f - 2.0f*x*x - 2.0f*y*y;
		_matrix[3][2] = 0.0f;

		_matrix[0][3] = 0.0f;
		_matrix[1][3] = 0.0f;
		_matrix[2][3] = 0.0f;
		_matrix[3][3] = 1.0f;
	}

	void Matrix4::CreateLookAt(const Vector3 & eye, const Vector3 &at, const Vector3 &up)
	{
		Vector3 front = at;
		front.Sub(eye);
		front.Normalize();

		Vector3 left = Cross(up, front);
		left.Normalize();

		Vector3 newUp = Cross(front, left);
		newUp.Normalize();

		_matrix[0][0] = left.GetX();
		_matrix[1][0] = left.GetY();
		_matrix[2][0] = left.GetZ();
		_matrix[3][0] = -1.0f * left.Dot(eye);

		_matrix[0][1] = newUp.GetX();
		_matrix[1][1] = newUp.GetY();
		_matrix[2][1] = newUp.GetZ();
		_matrix[3][1] = -1.0f * newUp.Dot(eye);

		_matrix[0][2] = front.GetX();
		_matrix[1][2] = front.GetY();
		_matrix[2][2] = front.GetZ();
		_matrix[3][2] = -1.0f * front.Dot(eye);

		_matrix[0][3] = 0.0f;
		_matrix[1][3] = 0.0f;
		_matrix[2][3] = 0.0f;
		_matrix[3][3] = 1.0f;
	}

	void Matrix4::CreateProjectionMatrix(int width, int height, float nearVal, float farVal, float FOV)
	{
		float aspectRatio = (float)width / (float)height;

		float yScale = (1.0f / tan((FOV / 2.0f)*(3.14159f / 180.0f))) *aspectRatio;
		float xScale = yScale / aspectRatio;

		memset(_matrix, 0, sizeof(float)* 16);
		_matrix[0][0] = xScale;
		_matrix[1][1] = yScale;
		_matrix[2][2] = -((farVal + nearVal) / (farVal - nearVal));
		_matrix[2][3] = -1.0f;
		_matrix[3][2] = -((2.0f * nearVal * farVal) / (farVal - nearVal));
	}
}