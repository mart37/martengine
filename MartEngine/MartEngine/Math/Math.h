#pragma once

#include "../Memory/Memory.h"
#include "../MartIncludes.h"

namespace MartEngine{

	class Vector3;
	class Quaternion;
	class Matrix4;

	class Vector3
	{
	private:
		float _x, _y, _z, _w;
	public:
		__forceinline Vector3() {}
		__forceinline Vector3(float x, float y, float z)
		{
			_x = x;
			_y = y;
			_z = z;
			_w = 1.0f;
		}

		__forceinline Vector3(const Vector3& rhs)
		{
			_x = rhs._x;
			_y = rhs._y;
			_z = rhs._z;
			_w = rhs._w;
		}

		__forceinline Vector3& operator=(const Vector3& rhs)
		{
			_x = rhs._x;
			_y = rhs._y;
			_z = rhs._z;
			_w = rhs._w;
			return *this;
		}

		__forceinline Vector3 operator+(const Vector3 &inB)
		{
			Vector3 toReturn = *this;
			toReturn.Add(inB);
			return toReturn;
		}

		__forceinline Vector3 operator+=(const Vector3 &inB)
		{
			this->Add(inB);
			return *this;
		}

		__forceinline Vector3 operator-(const Vector3 &inB)
		{
			Vector3 toReturn = *this;
			toReturn.Sub(inB);
			return toReturn;
		}

		__forceinline Vector3 operator-=(const Vector3 &inB)
		{
			this->Sub(inB);
			return *this;
		}

		__forceinline float GetX() const
		{
			return _x;
		}

		__forceinline float GetY() const
		{
			return _y;
		}

		__forceinline float GetZ() const
		{
			return _z;
		}

		__forceinline void Set(float x, float y, float z)
		{
			_x = x;
			_y = y;
			_z = z;
			_w = 1.0f;
		}

		__forceinline void SetX(float x)
		{
			_x = x;
		}

		__forceinline void SetY(float y)
		{
			_y = y;
		}

		__forceinline void SetZ(float z)
		{
			_z = z;
		}

		__forceinline float Dot(const Vector3& rhs) const
		{
			return (_x*rhs._x + _y*rhs._y + _z*rhs._z);
		}

		__forceinline void Add(const Vector3& rhs)
		{
			_x += rhs._x;
			_y += rhs._y;
			_z += rhs._z;
		}

		__forceinline void Sub(const Vector3 &rhs)
		{
			_x -= rhs._x;
			_y -= rhs._y;
			_z -= rhs._z;
		}

		__forceinline void Multiply(float scalar)
		{
			_x *= scalar;
			_y *= scalar;
			_z *= scalar;
		}

		__forceinline void Normalize()
		{
			float temp = _x*_x + _y*_y + _z*_z;
			temp = 1.0f / sqrtf(temp);

			_x *= temp;
			_y *= temp;
			_z *= temp;
		}

		__forceinline friend Vector3 Cross(const Vector3 &lhs, const Vector3 &rhs)
		{
			Vector3 result;
			result._x = lhs._y * rhs._z - lhs._z * rhs._y;
			result._y = lhs._z * rhs._x - lhs._x * rhs._z;
			result._z = lhs._x * rhs._y - lhs._y * rhs._x;
			return result;
		}

		void Rotate(const Quaternion& q);

		static const Vector3 Zero;
		static const Vector3 UnitX;
		static const Vector3 UnitY;
		static const Vector3 UnitZ;
		static const Vector3 NegativeUnitX;
		static const Vector3 NegativeUnitY;
		static const Vector3 NegativeUnitZ;
		static const Vector3 Right;
		static const Vector3 Up;
		static const Vector3 Forward;
		static const Vector3 Left;
		static const Vector3 Down;
		static const Vector3 Backward;
	};

	class Matrix4
	{
	private:
		float _matrix[4][4];
	public:
		__forceinline Matrix4() {}
		__forceinline Matrix4(float mat[4][4])
		{
			memcpy(_matrix, mat, sizeof(float) * 16);
		}

		__forceinline void Set(float mat[4][4])
		{
			memcpy(_matrix, mat, sizeof(float) * 16);
		}

		__forceinline Matrix4(const Matrix4& rhs)
		{
			memcpy(_matrix, rhs._matrix, sizeof(float) * 16);
		}

		__forceinline Matrix4& operator=(const Matrix4& rhs)
		{
			memcpy(_matrix, rhs._matrix, sizeof(float) * 16);
			return *this;
		}

		__forceinline void CreateScale(float scale)
		{
			memset(_matrix, 0, sizeof(float)* 16);
			_matrix[0][0] = scale;
			_matrix[1][1] = scale;
			_matrix[2][2] = scale;
			_matrix[3][3] = 1.0f;
		}

		void Multiply(const Matrix4& rhs)
		{
			float tmp[4][4];
			memcpy(tmp, _matrix, sizeof(float)* 16);

			// row 0
			_matrix[0][0] = tmp[0][0] * rhs._matrix[0][0] + tmp[0][1] * rhs._matrix[1][0] + tmp[0][2] * rhs._matrix[2][0] + tmp[0][3] * rhs._matrix[3][0];
			_matrix[0][1] = tmp[0][0] * rhs._matrix[0][1] + tmp[0][1] * rhs._matrix[1][1] + tmp[0][2] * rhs._matrix[2][1] + tmp[0][3] * rhs._matrix[3][1];
			_matrix[0][2] = tmp[0][0] * rhs._matrix[0][2] + tmp[0][1] * rhs._matrix[1][2] + tmp[0][2] * rhs._matrix[2][2] + tmp[0][3] * rhs._matrix[3][2];
			_matrix[0][3] = tmp[0][0] * rhs._matrix[0][3] + tmp[0][1] * rhs._matrix[1][3] + tmp[0][2] * rhs._matrix[2][3] + tmp[0][3] * rhs._matrix[3][3];

			// row 1
			_matrix[1][0] = tmp[1][0] * rhs._matrix[0][0] + tmp[1][1] * rhs._matrix[1][0] + tmp[1][2] * rhs._matrix[2][0] + tmp[1][3] * rhs._matrix[3][0];
			_matrix[1][1] = tmp[1][0] * rhs._matrix[0][1] + tmp[1][1] * rhs._matrix[1][1] + tmp[1][2] * rhs._matrix[2][1] + tmp[1][3] * rhs._matrix[3][1];
			_matrix[1][2] = tmp[1][0] * rhs._matrix[0][2] + tmp[1][1] * rhs._matrix[1][2] + tmp[1][2] * rhs._matrix[2][2] + tmp[1][3] * rhs._matrix[3][2];
			_matrix[1][3] = tmp[1][0] * rhs._matrix[0][3] + tmp[1][1] * rhs._matrix[1][3] + tmp[1][2] * rhs._matrix[2][3] + tmp[1][3] * rhs._matrix[3][3];

			// row 2
			_matrix[2][0] = tmp[2][0] * rhs._matrix[0][0] + tmp[2][1] * rhs._matrix[1][0] + tmp[2][2] * rhs._matrix[2][0] + tmp[2][3] * rhs._matrix[3][0];
			_matrix[2][1] = tmp[2][0] * rhs._matrix[0][1] + tmp[2][1] * rhs._matrix[1][1] + tmp[2][2] * rhs._matrix[2][1] + tmp[2][3] * rhs._matrix[3][1];
			_matrix[2][2] = tmp[2][0] * rhs._matrix[0][2] + tmp[2][1] * rhs._matrix[1][2] + tmp[2][2] * rhs._matrix[2][2] + tmp[2][3] * rhs._matrix[3][2];
			_matrix[2][3] = tmp[2][0] * rhs._matrix[0][3] + tmp[2][1] * rhs._matrix[1][3] + tmp[2][2] * rhs._matrix[2][3] + tmp[2][3] * rhs._matrix[3][3];

			// row 3
			_matrix[3][0] = tmp[3][0] * rhs._matrix[0][0] + tmp[3][1] * rhs._matrix[1][0] + tmp[3][2] * rhs._matrix[2][0] + tmp[3][3] * rhs._matrix[3][0];
			_matrix[3][1] = tmp[3][0] * rhs._matrix[0][1] + tmp[3][1] * rhs._matrix[1][1] + tmp[3][2] * rhs._matrix[2][1] + tmp[3][3] * rhs._matrix[3][1];
			_matrix[3][2] = tmp[3][0] * rhs._matrix[0][2] + tmp[3][1] * rhs._matrix[1][2] + tmp[3][2] * rhs._matrix[2][2] + tmp[3][3] * rhs._matrix[3][2];
			_matrix[3][3] = tmp[3][0] * rhs._matrix[0][3] + tmp[3][1] * rhs._matrix[1][3] + tmp[3][2] * rhs._matrix[2][3] + tmp[3][3] * rhs._matrix[3][3];
		}

		void CreateTranslation(const Vector3 &rhs);
		void CreateFromQuaternion(const Quaternion &q);
		void CreateProjectionMatrix(int width, int height, float nearVal, float farVal, float FOV);
		void CreateLookAt(const Vector3 & eye, const Vector3 &at, const Vector3 &up);

		const float* GetFloatArray() const { return &_matrix[0][0]; }

		static const Matrix4 Identity;
	};

	class Quaternion
	{
		friend class Vector3;
	private:
		Vector3 _qv;
		float _qs;
	public:
		Quaternion() {}
		Quaternion(const Vector3& axis, float angle) :
			_qv(axis)
		{
			float sinangleover2 = sinf(angle / 2.0f);
			_qv.Multiply(sinangleover2);
			_qs = cosf(angle / 2.0f);
		}

		__forceinline Quaternion(float qv_x, float qv_y, float qv_z, float qs)
		{
			_qv.Set(qv_x, qv_y, qv_z);
			_qs = qs;
		}

		__forceinline void Set(float qv_x, float qv_y, float qv_z, float qs)
		{
			_qv.Set(qv_x, qv_y, qv_z);
			_qs = qs;
		}

		__forceinline Quaternion(const Quaternion& rhs)
		{
			_qv = rhs._qv;
			_qs = rhs._qs;
		}

		__forceinline Quaternion& operator=(const Quaternion& rhs)
		{
			_qv = rhs._qv;
			_qs = rhs._qs;
			return *this;
		}

		void Multiply(const Quaternion &rhs)
		{
			Vector3 new_qv(_qv);
			new_qv.Multiply(rhs._qs);

			Vector3 temp(rhs._qv);
			temp.Multiply(_qs);

			new_qv.Add(temp);
			new_qv.Add(Cross(rhs._qv, _qv));

			_qs = rhs._qs * _qs - rhs._qv.Dot(_qv);
			_qv = new_qv;
		}

		Vector3 GetEulerAngles() {
			float qx = _qv.GetX();
			float qy = _qv.GetY();
			float qz = _qv.GetZ();
			float qw = _qs;

			float x = atan2f(2 * qx*qw - 2 * qy*qz, 1 - 2 * qx*qx - 2 * qz*qz);
			float y = atan2f(2 * qy*qw - 2 * qx*qz, 1 - 2 * qy*qy - 2 * qz*qz);
			float z = asinf(2 * qx*qy + 2 * qz*qw);

			return Vector3(x, y, z);
		}

		__forceinline float GetVectorX() const
		{
			return _qv.GetX();
		}

		__forceinline float GetVectorY() const
		{
			return _qv.GetY();
		}

		__forceinline float GetVectorZ() const
		{
			return _qv.GetZ();
		}

		__forceinline float GetScalar() const
		{
			return _qs;
		}
	};
}